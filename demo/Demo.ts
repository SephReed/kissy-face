

import { KissyFace } from "../src/OperationSets/KissyFace";
import { addRepoTools } from "../src/OperationSets/RepoTools/RepoTools.factory";
import { addGraphQL } from "../src/OperationSets/GraphQL/GraphQL.factory";

import { EndpointsManager } from "../src/Endpoints"

import knex from "knex";
import { join } from "path";
import express from "express";
// import { graphqlHTTP } from 'express-graphql';
import { GraphQLSchema } from "graphql/type/schema";
import { GraphQLObjectType } from "graphql/type/definition";
import { SelectRequired } from "../src/UtilityTypes/SelectRequired";
import { Repo } from "../src/OperationSets/RepoTools/Repo";
import { addDbSyncTool } from "../src/OperationSets/DbSyncTool";
import { apiErr } from "../src/Utils/apiErr";
import { RepoToolsConf } from "../src/OperationSets/RepoTools/RepoTools.io";
import { UnionToIntersection } from "../src/UtilityTypes/UnionToIntersection";
import { BaseConf } from "../src/OperationSets/Base/Base.io";



function createSchema<
	T extends object,
	ARGS extends Partial<T>,
	REPO extends Repo<T, ARGS> = Repo<T, ARGS>
>() {
	return KissyFace<T, ARGS>()([
		// addSQLite<T>(),
		addDbSyncTool<T>(),
		addRepoTools<T, ARGS, REPO>(),
		addGraphQL<T>(),
	])//(conf as any)
}


type User = {
	id: string;
	email: null | string;
	createdAt: Date;
	age: null | number;
	hasHat: null | boolean;
	// name?: { en: string };
}

type UserArgs = SelectRequired<User, "email">;



class UserRepo extends Repo<User, UserArgs>{
	public async create<BASE extends UserArgs>(createMe: BASE, forceValues?: "FORCE_VALUES") {
		console.log("HEY!!");
		return super.create(createMe, forceValues);
	}
}

// type test = RepoToolsConf<User, UserArgs, UserRepo>;
// type CONF = BaseConf<User, UserArgs> & UnionToIntersection<test>
// const a:CONF = {
// 	dbTableName: null as any,
// 	props: {
// 		id: {
// 			creationFill
// 		}
// 	}
// }

const UserSchema = createSchema<User, UserArgs, UserRepo>()({
	dbTableName: "User",
	primaryKey: "id",
	relations: () => ({
		hasMany: {
			Likes: { joinTo: LikeSchema.foreignKey("userId") }
		}
	}),
	customRepo: (schema) => new UserRepo(schema),
	props: {
		id: {
			nullable: false,
			unique: true,
			valueType: "text",
			creationFill: () => Date.now() + ""
		},
		createdAt: {
			nullable: false,
			valueType: "date",
			creationFill: () => new Date(),
		},
		email: {
			// unique: true,
			nullable: true as any,
			valueType: "text",
			// notNull: true,
			mustBeSpecified: true,
		},
		age: {
			nullable: true as any,
			valueType: "int",
			creationFill: () => 42,
		},
		hasHat: {
			nullable: true as any,
			valueType: "boolean",
			creationFill: () => false,
		}
		// name: {
		// 	valueType: "json"
		// }
	},
	syncPermits: [
		{ modifyColumn: "age", renameTo: "yearsAlive" },
		{ dropColumn: "yearsAlive", itWillBeDeleted: "I know" },
		{ addColumn: "age" },
		{ addColumn: "hasHat" } 
	],
});



type Like = {
	userId: string;
	createdAt: number;
	likeType: "wow" | "hot" | "poo-butt";
}


const LikeSchema = createSchema<Like, SelectRequired<Like, "userId" | "likeType">>()({
	dbTableName: "Like",
	primaryKey: null,
	multiColumnUniqueIndexes: [
		["likeType", "userId"]
	],
	props: {
		createdAt: {
			nullable: false,
			unique: true,
			creationFill: () => Date.now(),
			valueType: "big_int",
		},
		userId: {
			nullable: false,
			mustBeSpecified: true,
			valueType: { foreignKey: () => UserSchema.foreignKey("id")},
		},
		likeType: {
			nullable: false,
			mustBeSpecified: true,
			valueType: { enum: ["wow", "hot", "poo-butt"]},
		}
	},
	syncPermits: [
		{ dropUnique: "likeType,userId" },
		{ makeUnique: "likeType,userId" }
	],
})

// // console.log(out2._protected.opsConfig.graphQlId);
// // out2.gql();

const filename = join(__dirname, `./demo-db.sqlite`);
console.log(filename);

const db = knex({
  client: 'sqlite3',
  connection: {
    filename,
  },
	useNullAsDefault: true,
});

UserSchema.connect(db);
LikeSchema.connect(db);



;;(async () => {
	await UserSchema.dbSyncTool().sync();
	await LikeSchema.dbSyncTool().sync();

	const userRepo = UserSchema.repo;
	userRepo.get("", ["email"])
	let user = await userRepo.create({
		email: "yo@yoyo"
	});

	console.log(await userRepo.get(user.id));
	// let test = await UserRepo.require(user.id, ["email"]);
	// test.email

	// let test2 = await UserRepo.hydrate(test, ["createdAt"]);
	// test2.email;
	// test2.createdAt;

	// await UserRepo.syncObject(test);

	// test = await UserRepo.hydrate(test, ["createdAt"]),
	// test.createdAt.getDate();
	// test.id.length;
	const like = await LikeSchema.repo.create({
		userId: user.id,
		likeType: "wow",
	})
	console.log(like);
	// await UserRepo.updateWithObject(user, { email: "woop", name: { en: "woo" }})
	console.log(await userRepo.get(user.id));


	const app = express();
	const endpoints = EndpointsManager(app);
	app.use(express.urlencoded({ extended: true }))
	app.use(express.json());

	endpoints.add({
		path: "/test",
		methods: { get: { responses: {} } },
		cb: (req, res) => {
			throw apiErr("lalalla");
		}
	})



	endpoints.add({
		path: "/hello",
		cb: (req, res) => { res.send("Hello") },
		methods: {
			get: { responses: {} }
		}
	});

	endpoints.addOpenApiJson({
		baseDocs: {
			info: {
				title: "DEMO",
				version: "1"
			}
		}
	});

	endpoints.addSwaggerDocs({
		options: {
			ui: { siteTitle: "Demo 1"}
		}
	});

	// app.use(
	// 	'/graphql',
	// 	graphqlHTTP({
	// 		schema: new GraphQLSchema({
	// 			query: new GraphQLObjectType({
	// 				name: "Query",
	// 				fields: {
	// 					Users: UserSchema.genOpenQuery(),
	// 					Likes: LikeSchema.genOpenQuery(),
	// 				}
	// 			}),
	// 		}),
	// 		graphiql: true,
	// 	}),
	// );

	app.listen(4323);
})().catch((err) => console.error(err))



// abstract class Hat {
// 	public abstract hat(): 1;
// }

// abstract class Foo {
// 	public abstract foo(): 1;
// }

// abstract class Bar {
// 	public abstract bar(): 1;
// }

// const t = null as any as (Hat & Foo & Bar);

// const a = t.bar();
