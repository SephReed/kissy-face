import Fastify, { FastifyInstance, RouteShorthandOptions } from 'fastify'
import { EndpointsManager, generateServerDocs } from '../src/Endpoints'
import { apiErr } from '../src/Utils/apiErr'

const server: FastifyInstance = Fastify({})


const opts: RouteShorthandOptions = {
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          pong: {
            type: 'string'
          }
        }
      }
    }
  }
}

server.get('/ping', opts, async (request, reply) => {
  return { pong: 'it worked!' }
})

const endpoints = EndpointsManager(server);

endpoints.add({
  path: "/test",
  methods: { get: {} },
  cb: (req, res) => {
    res.send(generateServerDocs(server, {}));
    throw apiErr("lalalla");
  }
})

endpoints.add({
  path: "/lalal",
  methods: { post: {} },
  cb: (req, res) => {
    res.send(generateServerDocs(server, {}));
    throw apiErr("lalalla");
  }
})

endpoints.addOpenApiJson({ baseDocs: {
  info: {
    title: "demo",
    version: "11.2.3"
  }
}});

endpoints.addSwaggerDocs();



const start = async () => {
  try {
    const port = 4200;
    await server.listen({ port });
    console.log(`localhost:${port}`);
    console.log(server.printRoutes());


  } catch (err) {
    console.error(err);
    server.log.error(err)
    process.exit(1)
  }
}
start()