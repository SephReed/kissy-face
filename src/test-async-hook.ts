import { createHook, executionAsyncId, triggerAsyncId } from 'node:async_hooks';




class AsyncNodeManager<CTX extends object> {
  public byId = new Map<number, AsyncNode<CTX>>();

  constructor() {
    createHook({ 
      init: (id, type, parentId, resource) => {
        console.log("  " + parentId + " --> " + id + " " + type);
        // console.log(resource);
        const parent = this.byId.get(parentId);
        const addMe = new AsyncNode({
          manager: this,
          id,
          parent,
          type: type as any
        });
        this.byId.set(id, addMe);
        parent && parent.children.push(addMe);
      },
      // maybe should be destroy?
      destroy: (id) => {
        // console.log("resolve", id);
        const droppedIds = this.byId.get(id)?.drop();
        droppedIds?.forEach((id) => this.byId.delete(id));
      },
      // before: (id) => {
      //   console.log("to run", id);
      // }
    }).enable(); 

    const root = executionAsyncId();
    this.byId.set(root, new AsyncNode({
      manager: this,
      id: root,
      type: "TickObject"
    }));
  }


  public get currentNode() {
    const id = executionAsyncId();
    return this.byId.get(id)!;
  }
}

type AsyncNodeArgs<CTX extends object> = {
  id: number;
  parent?: AsyncNode<CTX>;
  type: "PROMISE" | "TickObject",
  manager: AsyncNodeManager<CTX>
}

class AsyncNode<CTX extends object> {
  constructor(public args: AsyncNodeArgs<CTX>) {
    // setters
  }

  public get id() { return this.args.id; }
  public get parent() { return this.args.parent; }

  protected _context?: CTX;
  public setContext(ctx: CTX) {
    this._context = ctx;
  }
  public getContext(): CTX | undefined {
    if (this._context) {
      return this._context;
    }
    return this.parent?.getContext();
  }

  public children: AsyncNode<CTX>[] = [];

  public drop(): number[] {
    return [
      this.id,
      this.children.map((it) => it.drop())
    ].flat(2);
  }
}

const manager = new AsyncNodeManager<{userId: string}>();

// forces PromiseHooks to be enabled.


function log(extra: string = "") {
  const node = manager.currentNode;
  console.log(extra + node.id + " :: ", node.getContext())
}

async function run() {
  // await Promise.resolve(1729);
  log("Root -- ");

  // await new Promise((res) => {
  //   manager.currentNode.setContext({ userId: "seph" });
  //   console.log("Set within -- ", manager.currentNode.id);
  //   res(null);
  // })
  // // await Promise.all([
  // //   (async () => {
  // //     await Promise.resolve();
  // //     manager.currentNode.setContext({ userId: "seph" });
  // //     console.log("Set within -- ", manager.currentNode.id);
  // //     await Promise.resolve();
  // //     log("Branch 1 End -- ")
  // //   })(),
  // //   (async () => {
  // //     log("Branch 2 -- ");
  // //   })()
  // // ]);
  // // await Promise.resolve();
  // log("End -- ");
}

Promise.resolve().then(() => {
  manager.currentNode.setContext({ userId: "seph" });
  // console.log("Set within -- ", manager.currentNode.id);
  return run();
})

Promise.resolve().then(() => {
  manager.currentNode.setContext({ userId: "booba" });
  // console.log("Set within -- ", manager.currentNode.id);
  return run();
})

// .then(() => {
//   console.log("DONE");
// });

// setTimeout(() => {
//   console.log("AFTER");
//   console.log(Array.from(manager.byId.keys()))
// }, 1500);


// Promise.resolve(1729).then(() => {
//   console.log("Out 3", manager.currentNode.getContext());
// })