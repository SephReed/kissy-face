export type SubpropertyMerge<T> = (
	T extends object ? (
		T extends string | number | ((...args: any) => any) | symbol | boolean ? T
		: { [K in keyof T]: SubpropertyMerge<T[K]> }
	) : T
);
