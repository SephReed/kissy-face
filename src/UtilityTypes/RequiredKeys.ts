import { KeyOf } from "./KeyOf";

export type RequiredKeys<T> = { [K in KeyOf<T>]-?: {} extends Pick<T, K> ? never : K }[KeyOf<T>];
export type OptionalKeys<T> = { [K in KeyOf<T>]-?: {} extends Pick<T, K> ? K : never }[KeyOf<T>];



export type RequiredOnly<T extends object> = { [K in RequiredKeys<T>]: T[K] };
export type OptionalOnly<T extends object> = { [K in RequiredKeys<T>]: T[K] };


// export type ConvertOptionalsToNulls<T extends object> = RequiredOnly<T> & { [KEY in OptionalKeys<T>]: null | Exclude<T[KEY], undefined> }



// type Test = { a: 1, b?: 2};

// let a: RequiredKeys<Test>;

// let b: RequiredOnly<Test> = null as any;
// b.a;
// b.b;
