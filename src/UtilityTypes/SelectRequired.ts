

export type SelectRequired<T extends object, KEYS extends keyof T> = Partial<T> & Required<Pick<T, KEYS>>;
