import { KeyOf } from "./KeyOf";

export type NullableKeys<T> = { [KEY in KeyOf<T>]-?: Extract<T[KEY], null> extends never ? never : KEY }[KeyOf<T>];
export type NonNullableKeys<T> = { [KEY in KeyOf<T>]-?: Extract<T[KEY], null> extends never ? KEY : never }[KeyOf<T>];
