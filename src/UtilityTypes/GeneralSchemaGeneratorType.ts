import { RequiredKeys } from "./RequiredKeys";
import { SubpropertyMerge } from "./SubpropertyMerge";

export type GeneralSchemaGenerator<
	T extends object,
	INSERTABLE extends Partial<T>,
	CREATE_CONFIG extends Object,
	NOT_NULL_CONFIG extends Object,
	DEFAULT_CONFIG extends Object,
	NOT_NULL_SPECIFIED_CONFIG = {}, DEFAULT_SPECIFIED_CONFIG = {}
> = KeySpecifiedSchemaGenerator<
	RequiredKeys<INSERTABLE>, CREATE_CONFIG,
	RequiredKeys<T>, NOT_NULL_CONFIG,
	keyof T, DEFAULT_CONFIG,
	NOT_NULL_SPECIFIED_CONFIG, DEFAULT_SPECIFIED_CONFIG
>





export type KeyType = string | number | symbol;

export type KeySpecifiedSchemaGenerator<
	CREATE_KEYS extends KeyType, CREATE_CONFIG extends Object,
	NOT_NULL_KEYS extends KeyType, NOT_NULL_CONFIG extends Object,
	ALL_KEYS extends KeyType, DEFAULT_CONFIG extends Object,
	NOT_NULL_SPECIFIED_CONFIG = {}, DEFAULT_SPECIFIED_CONFIG = {}
> = SubpropertyMerge<
	GroupConfig<CREATE_KEYS, CREATE_CONFIG>
	& GroupConfig<NOT_NULL_KEYS, NOT_NULL_CONFIG>
	& GroupConfig<ALL_KEYS, DEFAULT_CONFIG>
	& GroupSpecificConfig<CREATE_KEYS, NOT_NULL_KEYS, NOT_NULL_SPECIFIED_CONFIG>
	& GroupSpecificConfig<NOT_NULL_KEYS, ALL_KEYS, DEFAULT_SPECIFIED_CONFIG>
>;



type GroupConfig<GROUP extends KeyType, CONFIG> = {
  [key in GROUP]: CONFIG
}


type GroupSpecificConfig<
	GROUP extends KeyType, ANTI_GROUP extends KeyType,
	CONFIG,
	SPECIFICS extends Exclude<ANTI_GROUP, GROUP> = Exclude<ANTI_GROUP, GROUP>
> = {
	[key in SPECIFICS]: CONFIG
}




// export type GeneralSchemaGenerator<
// 	INSERTABLE, NOT_NULL, DEFAULT_GROUP,
// 	INSERTABLE_CONFIG, NOT_NULL_CONFIG, DEFAULT_CONFIG,
// 	NOT_NULL_SPECIFIED_CONFIG = {}, DEFAULT_SPECIFIED_CONFIG = {}
// > = SubpropertyMerge<
// 	GroupConfig<INSERTABLE, INSERTABLE_CONFIG>
// 	& GroupConfig<NOT_NULL, NOT_NULL_CONFIG>
// 	& GroupConfig<DEFAULT_GROUP, DEFAULT_CONFIG>
// 	& GroupSpecificConfig<NOT_NULL, INSERTABLE, NOT_NULL_SPECIFIED_CONFIG>
// 	& GroupSpecificConfig<DEFAULT_GROUP, NOT_NULL, DEFAULT_SPECIFIED_CONFIG>
// >;


// type SubpropertyMerge<T> = T extends (...args: infer A) => infer R
// ? (...args: SubpropertyMerge<A>) => SubpropertyMerge<R>
// : T extends object ? { [K in keyof T]: SubpropertyMerge<T[K]> } : T;


// type GroupConfig<GROUP, CONFIG> = {
//   [key in keyof GROUP]: CONFIG
// }


// type GroupSpecificConfig<GROUP, ANTI_GROUP, CONFIG, SPECIFICS = Omit<GROUP, keyof ANTI_GROUP>> = {
// 	[key in keyof SPECIFICS]: CONFIG
// }
