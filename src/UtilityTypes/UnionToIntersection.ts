import { SubpropertyMerge } from "./SubpropertyMerge";

// export type UnionToIntersection<U> = [U] extends [infer I] ? I : never

export type UnionToIntersection<U> = (
  U extends any ? (k: U) => void : never
) extends (k: infer I) => void
  ? I
  : never

export type DeepIntersectUnion<U> = SubpropertyMerge<UnionToIntersection<U>>;
