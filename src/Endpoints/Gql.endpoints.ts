import { GraphQLObjectType } from "graphql/type/definition";
import { GraphQLSchema } from "graphql/type/schema";
import { BaseOps, BaseConf } from "../OperationSets/Base/Base.io"
import { GraphQlOps } from "../OperationSets/GraphQL/GraphQL.io"


type GqlSchema = BaseOps<{}, BaseConf<{}, {}>> & GraphQlOps;

export interface IGqlEndpointManagerArgs {
	schemas: Array<GqlSchema | {
		schema: GqlSchema;
		uniqueSelectionName?: string;
		pluralSelectionName?: string;
	}>;
}


export class GqlEndpointManager {
	constructor(protected args: IGqlEndpointManagerArgs) {

	}

	protected gqlSchema: GraphQLSchema = undefined as any;
	public createGqlSchema() {
		if (this.gqlSchema) { return this.gqlSchema; }
		const fields: any = {};
		this.args.schemas.forEach((it) => {
			const schema = "schema" in it ? it.schema : it;
			const specified = it !== schema;
			let pluralName = ("pluralSelectionName" in it && it.pluralSelectionName )? it.pluralSelectionName : schema._protected.config.dbTableName;
			if (!specified && pluralName.charAt(pluralName.length-1) !== "s") {
				pluralName += "s";
			}

			fields[pluralName] = schema.genOpenQuery();
		})

		return this.gqlSchema = new GraphQLSchema({
			query: new GraphQLObjectType({
				name: "Query",
				fields
			}),
		});
	}

	// public convertGqlJsonToQueryString(_gqlJson: object) {
	// 	const gqlJson = {
	// 		query: {
	// 			Roles: {
	// 				where: { type: "member", holderId: "2"},
	// 				joins:
	// 			}
	// 		}
	// 	}
	// }

	// public createGqlJsonEndpoint(): IEndpointArgs {
	// 	if (!this.gqlSchema) {
	// 		throw new Error(`createGqlSchema must be run before creating a JSON endpoint`)
	// 	}
	// 	graphql({
	// 		schema: this.gqlSchema,
	// 		source: ""
	// 	})
	// 	return null as any;
	// }
}
