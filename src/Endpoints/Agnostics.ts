import type { FastifyInstance } from "fastify/types/instance";

export function isFastify(checkMe: any): checkMe is FastifyInstance {
  return checkMe 
    && typeof checkMe === "object" 
    && "removeAllContentTypeParsers" in checkMe
    && "setValidatorCompiler" in checkMe
}