import { readFileSync } from "fs";
import { join } from "path";
import swaggerUi from "swagger-ui-dist";

export interface JsonObject {
  [key: string]: any;
}

export type SwaggerOptions = {
  [key: string]: any;
} & ({ swaggerUrl: string; } | { swaggerUrls: string[];})

export type SwaggerUiOptions = {
  css?: string;
  cssUrl?: string;
  favIcon?: string;
  js?: string;
  siteTitle?: string;
  _htmlTplString?: string, 
  _jsTplString?: string,
  isExplorer?: boolean;
} 

export interface GenerateArgs {
  swaggerDoc?: JsonObject,
  ui?: SwaggerUiOptions,
  options: SwaggerOptions,
}

const FAV_ICON_HTML = '<link rel="icon" type="image/png" href="./favicon-32x32.png" sizes="32x32" />' +
  '<link rel="icon" type="image/png" href="./favicon-16x16.png" sizes="16x16" />'


const HTML_TEMPLATE = `
<!-- HTML for static distribution bundle build -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><% title %></title>
  <% swagger-ui-css %>
  <% favIconString %>
  <style>
    html
    {
      box-sizing: border-box;
      overflow: -moz-scrollbars-vertical;
      overflow-y: scroll;
    }
    *,
    *:before,
    *:after
    {
      box-sizing: inherit;
    }
    body {
      margin:0;
      background: #fafafa;
    }
  </style>
</head>
<body>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="position:absolute;width:0;height:0">
  <defs>
    <symbol viewBox="0 0 20 20" id="unlocked">
      <path d="M15.8 8H14V5.6C14 2.703 12.665 1 10 1 7.334 1 6 2.703 6 5.6V6h2v-.801C8 3.754 8.797 3 10 3c1.203 0 2 .754 2 2.199V8H4c-.553 0-1 .646-1 1.199V17c0 .549.428 1.139.951 1.307l1.197.387C5.672 18.861 6.55 19 7.1 19h5.8c.549 0 1.428-.139 1.951-.307l1.196-.387c.524-.167.953-.757.953-1.306V9.199C17 8.646 16.352 8 15.8 8z"></path>
    </symbol>
    <symbol viewBox="0 0 20 20" id="locked">
      <path d="M15.8 8H14V5.6C14 2.703 12.665 1 10 1 7.334 1 6 2.703 6 5.6V8H4c-.553 0-1 .646-1 1.199V17c0 .549.428 1.139.951 1.307l1.197.387C5.672 18.861 6.55 19 7.1 19h5.8c.549 0 1.428-.139 1.951-.307l1.196-.387c.524-.167.953-.757.953-1.306V9.199C17 8.646 16.352 8 15.8 8zM12 8H8V5.199C8 3.754 8.797 3 10 3c1.203 0 2 .754 2 2.199V8z"/>
    </symbol>
    <symbol viewBox="0 0 20 20" id="close">
      <path d="M14.348 14.849c-.469.469-1.229.469-1.697 0L10 11.819l-2.651 3.029c-.469.469-1.229.469-1.697 0-.469-.469-.469-1.229 0-1.697l2.758-3.15-2.759-3.152c-.469-.469-.469-1.228 0-1.697.469-.469 1.228-.469 1.697 0L10 8.183l2.651-3.031c.469-.469 1.228-.469 1.697 0 .469.469.469 1.229 0 1.697l-2.758 3.152 2.758 3.15c.469.469.469 1.229 0 1.698z"/>
    </symbol>
    <symbol viewBox="0 0 20 20" id="large-arrow">
      <path d="M13.25 10L6.109 2.58c-.268-.27-.268-.707 0-.979.268-.27.701-.27.969 0l7.83 7.908c.268.271.268.709 0 .979l-7.83 7.908c-.268.271-.701.27-.969 0-.268-.269-.268-.707 0-.979L13.25 10z"/>
    </symbol>
    <symbol viewBox="0 0 20 20" id="large-arrow-down">
      <path d="M17.418 6.109c.272-.268.709-.268.979 0s.271.701 0 .969l-7.908 7.83c-.27.268-.707.268-.979 0l-7.908-7.83c-.27-.268-.27-.701 0-.969.271-.268.709-.268.979 0L10 13.25l7.418-7.141z"/>
    </symbol>
    <symbol viewBox="0 0 24 24" id="jump-to">
      <path d="M19 7v4H5.83l3.58-3.59L8 6l-6 6 6 6 1.41-1.41L5.83 13H21V7z"/>
    </symbol>
    <symbol viewBox="0 0 24 24" id="expand">
      <path d="M10 18h4v-2h-4v2zM3 6v2h18V6H3zm3 7h12v-2H6v2z"/>
    </symbol>
  </defs>
</svg>
<div id="swagger-ui"></div>
<% swagger-ui-bundle %>
<% swagger-ui-standalone-preset %>
<script><% swagger-ui-init %></script>
<% customJs %>
<% customCssUrl %>
<style>
  <% customCss %>
</style>
</body>
</html>
`

const JS_INIT_TEMPLATE = `
window.onload = function() {
  // Build a system
  <% swaggerOptions %>
  var url = options.swaggerUrl;
  if (!url) {
    url = window.location.search.match(/url=([^&]+)/);
    if (url && url.length > 1) {
      url = decodeURIComponent(url[1]);
    } else {
      url = window.location.origin;
      console.warn("options.swaggerUrl not set, defaulting to origin")
    }
  }
  var urls = options.swaggerUrls
  var customOptions = options.customOptions
  var spec1 = options.swaggerDoc
  var swaggerOptions = {
    spec: spec1,
    url: url,
    urls: urls,
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
  }
  for (var attrname in customOptions) {
    swaggerOptions[attrname] = customOptions[attrname];
  }
  var ui = SwaggerUIBundle(swaggerOptions)
  if (customOptions.oauth) {
    ui.initOAuth(customOptions.oauth)
  }
  if (customOptions.authAction) {
    ui.authActions.authorize(customOptions.authAction)
  }
  window.ui = ui
}
`


function stringifyOptions(options: {}) {
  const placeholder = '____FUNCTIONPLACEHOLDER____'
  const fns: Array<Function> = [];
  let json = JSON.stringify(options, (_, value) => {
    if (typeof value === 'function') {
      fns.push(value);
      return placeholder;
    }
    return value;
  }, 2)
  json = json.replace(new RegExp(`"${placeholder}"`, 'g'), (_) => fns.shift()?.toString() || "")
  return 'var options = ' + json + ';'
}

export function generateHTML(args: GenerateArgs) {
  const { ui, options } = args;

  //support legacy params based function
  const isExplorer = ui?.isExplorer || !!options?.swaggerUrls;

  const initOptions = {
    swaggerDoc: args.swaggerDoc,
    customOptions: args.options || {},
    swaggerUrl: options?.swaggerUrl,
    swaggerUrls: options?.swaggerUrls
  }

  const swaggerInit = (ui?._jsTplString || JS_INIT_TEMPLATE).replace('<% swaggerOptions %>', stringifyOptions(initOptions));

  const explorerString = isExplorer ? '' : '.swagger-ui .topbar .download-url-wrapper { display: none }';

  //`<script src="./swagger-ui-bundle.js"></script>`
  const UIBundleJS = (readFileSync(join(swaggerUi.absolutePath(), "/swagger-ui-bundle.js")));
  //`<script src="./swagger-ui-standalone-preset.js"></script>`
  const StandalonePresetJS = (readFileSync(join(swaggerUi.absolutePath(), "/swagger-ui-standalone-preset.js")));
  // <link rel="stylesheet" type="text/css" href="./swagger-ui.css" >
  const SwaggerCss = readFileSync(join(swaggerUi.absolutePath(), "/swagger-ui.css"));

  return (ui?._htmlTplString || HTML_TEMPLATE)
    .replace('<% customCss %>', `${explorerString} ${ui?.css || ''}`)
    .replace('<% favIconString %>', ui?.favIcon ? `<link rel="icon" href="${ui.favIcon}" />` : FAV_ICON_HTML)
    .replace('<% customJs %>', ui?.js ? `<script src="${ui.js}"></script>` : '')
    .replace('<% customCssUrl %>', ui?.cssUrl ? `<link href="${ui.cssUrl}" rel="stylesheet">` : '')
    .replace('<% title %>', ui?.siteTitle || "Swagger UI")
    .replace('<% swagger-ui-init %>', swaggerInit)
    .replace(`<% swagger-ui-bundle %>`, `<script>${UIBundleJS}</script>`)
    .replace(`<% swagger-ui-standalone-preset %>`, `<script>${StandalonePresetJS}</script>`)
    .replace(`<% swagger-ui-css %>`, `<style>${SwaggerCss}</style>`)
}