
import type { Application } from "express";
import type { FastifyInstance, RouteOptions } from "fastify";
import { isFastify } from "./Agnostics"
import { OpenAPIV3 } from "openapi-types";



export interface IRouteDoc extends Omit<
	OpenAPIV3.PathItemObject, 
	"get" | "put" | "post" | "delete" | "options" | "head" | "patch" | "trace"
> {
	get?: IEndpointDoc;
	put?: IEndpointDoc;
	post?: IEndpointDoc;
	delete?: IEndpointDoc;
	options?: IEndpointDoc;
	head?: IEndpointDoc;
	patch?: IEndpointDoc;
	trace?: IEndpointDoc;
}

// Describes the endpoint
export interface IEndpointDoc extends Partial<OpenAPIV3.OperationObject> {
	requestBodyJson?: IJsonRequestBody | { __example: object };
	authorization?: IEndpointAuthorization;
	// dontStoreOutputInEvent?: boolean;
}

export type IEndpointAuthorization =  ISpecialEndpointAuth;  // | ISimpleEndpointAuth

// export interface ISimpleEndpointAuth {
// 	type?: "Bearer";
// 	blockAdmin?: boolean;
// 	matchAny?: Array<IKissyfaceAuth>
// }

export interface ISpecialEndpointAuth {
	type?: "Bearer";
	handledByMiddleware: true;
}



// Describes the request body for POST/PATCH/etc
export interface IJsonRequestBody {
	[key: string]: OpenAPIV3.NonArraySchemaObjectType | OpenAPIV3.SchemaObject
}




export function createJsonRequestBodyFromExampleObject(obj: any) {
	const out: IJsonRequestBody = {};
	Array.from(Object.entries(obj)).forEach(([key, value]) => {
		let type = typeof value;
		switch (type) {
			case "bigint": type = "number"; break;
			case "string":
			case "object":
			case "number":
			case "boolean": break;
			default: throw new Error(`TODO: make sure ${type} is valid for open api schema type`);
		}

		// if (type !== "string") {
		// 	throw new Error(`TODO: make sure ${type} is valid for open api schema type`);
		// }
		out[key] = {
			example: value,
			type
		}
	});
	return out;
}




export function generateServerDocs(
	app: Application | FastifyInstance, 
	baseDocs: Partial<OpenAPIV3.Document>
) {
	// baseline docs
	const out: OpenAPIV3.Document = {
		openapi: "3.0.0",
		info: {
			version: "0",
			title: "Unamed API"
		},
		paths: {},
		components: {
			schemas: {},
			securitySchemes: {
				bearerAuth: {
					type: "http",
					scheme: "bearer",
				}
			}
		},
		...baseDocs
	};

	const items = [] as Array<{
    path: string;
    docs: IRouteDoc;
	}>;

	if (isFastify(app)) {
		const itemMap = new Map<string, IRouteDoc>();
		((<any>app)._kissyRoutes as RouteOptions[]).forEach((route) => {
			let path = route.url || (route.handler as any).path;
			if (!path || itemMap.has(path)) { return; }

			const doc = (route.handler as any).docs = (route.handler as any).docs || { get: {} };
			itemMap.set(path, doc);
		});
		for (const [path, docs] of itemMap.entries()) {
			items.push({path, docs});
		}

	} else {
		// find all registered endpoints, create OpenApi doc for each
		(app._router.stack as any[]).forEach((layer) => {
			let path = (layer.route && layer.route.path) || layer.handle.path;
			if (!path) { return; }

			// docs variable is added by registerEndpoint, it is special to this framework
			// by default, endpoints are treated as an undescribed get
			items.push({ 
				path, 
				docs:  layer.handle.docs = layer.handle.docs || { get: {} }
			})
		});
	}


	items.forEach((args) => {
		let { path, docs} = args;

		const params: string[] = [];
		path = path.replace(/:([^\/]+)/g, (_: unknown, g1: string) => {
			params.push(g1);
			return `{${g1}}`
		})

		// this is where OpenApi formated docs are built for each path
		const methods = out.paths[path] = out.paths[path] || {};

		Object.entries(docs).forEach(([_method, methodDoc] : [string, IEndpointDoc]) => {
			const method = _method as keyof IRouteDoc;
			if (methods[method] !== undefined) {
				throw new Error("Double defining api method");
			}
			// @ts-ignore
			methods[method] = methodDoc;

			if (!methodDoc.responses) {
				methodDoc.responses = {};
			}

			// a framework specific shorthand for describing a request body
			if (methodDoc.requestBodyJson) {
				const id = path.replace(/\//g, "__");
				// @ts-ignore
				const ref: OpenAPIV3.SchemaObject = out.components.schemas[id] = {
					type: "object",
					required: [],
					properties: {}
				};

				methodDoc.requestBody = {
					required: true,
					content: {
						"application/json": {
							schema: {
								$ref: `#/components/schemas/${id}`
							}
						}
					}
				};

				let jsonRequestBody: IJsonRequestBody;
				if ("__example" in methodDoc.requestBodyJson) {
					jsonRequestBody = createJsonRequestBodyFromExampleObject(methodDoc.requestBodyJson.__example);
				} else {
					jsonRequestBody = methodDoc.requestBodyJson;
				}

				Object.entries(jsonRequestBody).forEach(([propName, propDoc]) => {
					const props = ref.properties = ref.properties || {};
					if (typeof propDoc === "string") {
						props[propName] = { type: propDoc }
					} else {
						props[propName] = propDoc;
					}
				})

				delete methodDoc.requestBodyJson;
			}

			// actual authorization is done during the endpoint call
			const auth = methodDoc.authorization;
			if (auth) {
				if (auth.type === "Bearer" || !auth.type) {
					methodDoc.security = [{
						bearerAuth: []
					}]
				}
			}

			// ..something to do with /path/to/{{userId}}/name
			const methodParams = methodDoc.parameters = methodDoc.parameters || [];
			params.forEach((params) => {
				if (methodParams.find((it) => !("$ref" in it) && it.name === params)) {
					return;
				}
				methodParams.push({
					name: params,
					in: "path",
					required: true,
				});
			})

			const methodResponses = methodDoc.responses = methodDoc.responses || {};
			if (!methodResponses["200"]) {
				methodResponses["200"] = {
					description: "Default Success Response"
				}
			}
		});
	})

	return out;
}
