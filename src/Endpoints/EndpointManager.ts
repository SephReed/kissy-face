import type { Application, Request, Response } from "express";
import type { FastifyInstance, FastifyRequest, FastifyReply, HTTPMethods } from "fastify";


import { OpenAPIV3 } from "openapi-types";
import { apiErr, isApiError } from "../Utils/apiErr";
import { generateServerDocs, IEndpointDoc, IRouteDoc } from "./ServerDocs";
import { isFastify } from "./Agnostics";
import { generateHTML, SwaggerOptions, SwaggerUiOptions } from "./SwaggerTool"




export type EndpointCb<REQ, RES> = (req: REQ, res: RES) => void | Promise<void>;

export interface IMiddlewareItem<REQ, RES> {
	methods: {
		[key in keyof IRouteDoc]?: EndpointCb<REQ, RES>;
	};
	middleware: EndpointCb<REQ, RES>;
}

export interface IEndpointArgs<REQ, RES> {
	path: string,
	cb: EndpointCb<REQ, RES>,
	methods: IRouteDoc
}

export type FastifyEndpointsManager = _EndpointsManager<FastifyRequest, FastifyReply, FastifyInstance>;
export type ExpressEndpointsManager = _EndpointsManager<Request, Response, Application>;

export type FastifyEndpointArgs = IEndpointArgs<FastifyRequest, FastifyReply>;
export type ExpressEndpointArgs = IEndpointArgs<Request, Response>;


export function EndpointsManager(app: Application): ExpressEndpointsManager;
export function EndpointsManager(app: FastifyInstance): FastifyEndpointsManager;
export function EndpointsManager(
	app: Application | FastifyInstance
): FastifyEndpointsManager | ExpressEndpointsManager {
	if (isFastify(app)) {
		const routes = (<any>app)._kissyRoutes = [] as any[];
		app.addHook('onRoute', route => {
      routes.push(route);
    });
		return new _EndpointsManager<FastifyRequest, FastifyReply, FastifyInstance>({
			app,
			addPathToApplication: (method, url, handler) => {
				app.route({ 
					method,
					url, 
					handler,
				})
			},
		});
	}
	return new _EndpointsManager<Request, Response, Application>({
		app,
		addPathToApplication: (_, path, cb) => app.use(path, cb)
	});
}



class _EndpointsManager<
	REQ extends Request | FastifyRequest,
	RES extends Response | FastifyReply,
	APP extends Application | FastifyInstance
> {
	constructor(protected args: {
		addPathToApplication: (methods: HTTPMethods[], path: string, cb: EndpointCb<REQ, RES>) => void;
		app: APP
	}) {
		// no inits
	}

	public get app() { return this.args.app; }

	/** create a new endpoint, with optional docs info
	 * method types are defined in the docs arg
	 */
	public add(args: IEndpointArgs<REQ, RES> | IEndpointArgs<REQ, RES>[]) {
		// any cached server docs should be re-rendered
		this.serverDocs = undefined;

		if (Array.isArray(args)) {
			args.forEach((arg) => this.add(arg));
			return;
		}
		const {cb, methods} = args;

		const middlewareItem = this.getnitMiddlewareItem(args.path);
		const routeDocs = (middlewareItem.middleware as any).docs;
		const methodTypes = Array.from(Object.keys(methods)) as Array<keyof IRouteDoc>;
		if (!methodTypes.length) { throw new Error(`At least one method must be described to add route to "${args.path}"`); }
		methodTypes.forEach((methodType) => {
			if (methodType in routeDocs) {
				throw apiErr(`Method "${methodType}" is double defined for route "${args.path}"`);
			}
			routeDocs[methodType] = methods[methodType];
			middlewareItem.methods[methodType] = cb;
		});

		const methodNames = methodTypes.map((it) => it.toUpperCase()) as HTTPMethods[];
		this.args.addPathToApplication(methodNames, args.path, middlewareItem.middleware);
	}


	// create an item to describe the
	protected middlewareItems = new Map<string, IMiddlewareItem<REQ, RES>>();
	protected getnitMiddlewareItem(path: string) {
		if (this.middlewareItems.has(path)) {
			return this.middlewareItems.get(path) as IMiddlewareItem<REQ, RES>;
		}

		const middlewareItem: IMiddlewareItem<REQ, RES> = {
			middleware: async (req, res) => {
				try {
					// console.log("runEndpointCall")
					await this.runEndpointCall({
						path,
						req,
						res,
						middlewareItem
					})
				} catch(err) {
					if (isApiError(err) && err.status < 500) {
						res.status(err.status);
						res.send(`${err.status} Error: ${err.message}`);
						return;
					}
					console.error("Internal Endpoint ERR:", err);
					res.status(500);
					res.send();
				}
			},
			methods: {}
		};
		this.middlewareItems.set(path, middlewareItem);

		// these variables are stored with the express layer
		// later, when docs are being generated, the entire express
		// app is parsed to create docs.  Any layer with special docs
		// like this will have them displayed
		(middlewareItem.middleware as any).path = path;
		(middlewareItem.middleware as any).docs = {};
		

		return middlewareItem;
	}



	protected async runEndpointCall(args: {
		path: string;
		req: REQ;
		res: RES;
		middlewareItem: IMiddlewareItem<REQ, RES>;
	}) {
		const { path, middlewareItem, req, res } = args;
		const { method } = req;

		const _res = res as Response & FastifyReply;
		const _send = _res.send.bind(res);
		let sendBody: any;
		// hack to capture response send body
		_res.send = (body: any) => {
			sendBody = body;
			return _send(body);
		}

		const methodType = method.toLowerCase() as keyof IRouteDoc;
		const docs = (middlewareItem.middleware as any).docs;
		const methodDocs: IEndpointDoc = docs && docs[methodType];
		if (docs && !methodDocs) {
			throw apiErr(`Invalid method "${method}" for endpoint "${path}"`);
		}

		// authorize call
		// const { authorization } = methodDocs;
		// if (authorization && this.authorize(authorization, req.kissyface.auth) === false) {
		// 	throw apiErr(401, `Invalid or missing authorization`);
		// }

		// run middleware
		const methodCb = middlewareItem.methods[methodType];
		methodCb && (await methodCb(req, res));
		return sendBody;
	}


	protected openApiPath?: string;
	protected serverDocs?: OpenAPIV3.Document<{}>;
	public addOpenApiJson(args: {
		path?: string,
		baseDocs: Partial<OpenAPIV3.Document<{}>> & Pick<OpenAPIV3.Document<{}>, "info">,
	}) {
		const path = args.path || "/docs/open-api.json";
		this.openApiPath = path;
		this.args.addPathToApplication(["GET"], path, (req, resp) => {
			this.serverDocs = this.serverDocs || generateServerDocs(this.app, args.baseDocs);
			resp.send(JSON.stringify(this.serverDocs, null, 2))
		});
	}


	public addSwaggerDocs(args: {
		path?: string,
		addOpenApiJson?: {
			path?: string,
			baseDocs: Partial<OpenAPIV3.Document<{}>> & Pick<OpenAPIV3.Document<{}>, "info">,
		},
		options?: {
			swagger?: SwaggerOptions,
			ui?: SwaggerUiOptions
		}
	} = {}) {
		if (!this.openApiPath) {
			if (!args.addOpenApiJson) {
				throw new Error(`Open API path must be created for swagger docs.  Use either "addOpenApiPath" arg, or run Endpoints.addOpenApiJson() first.`)
			} else {
				this.addOpenApiJson(args.addOpenApiJson);
			}

		} else if (args.addOpenApiJson && args.addOpenApiJson.path) {
			throw new Error(`Open API path has already been defined as "${this.openApiPath}"`);
		}

		const path = args.path || "/docs";

		const html = generateHTML({
			options: {
				swaggerUrl: this.openApiPath!,
				...args.options?.swagger
			},
			ui: args.options?.ui
		})

		this.args.addPathToApplication(["GET"], path, (req, res) => {
			res.type(".html");
			res.send(html);
		})
	}

	
}



