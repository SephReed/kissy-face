import { createHook, executionAsyncId } from 'node:async_hooks';

export class AsyncNodeManager<CTX extends object> {
  public byId = new Map<number, AsyncNode<CTX>>();

  constructor() {
    createHook({ 
      init: (id, type, parentId, resource) => {
        console.log("  " + parentId + " --> " + id + " " + type);
        // console.log(resource);
        const parent = this.byId.get(parentId);
        const addMe = new AsyncNode({
          manager: this,
          id,
          parent,
          type: type as any
        });
        this.byId.set(id, addMe);
        parent && parent.children.push(addMe);
      },
      // maybe should be destroy?
      destroy: (id) => {
        // console.log("resolve", id);
        const droppedIds = this.byId.get(id)?.drop();
        droppedIds?.forEach((id) => this.byId.delete(id));
      },
      // before: (id) => {
      //   console.log("to run", id);
      // }
    }).enable(); 

    const root = executionAsyncId();
    this.byId.set(root, new AsyncNode({
      manager: this,
      id: root,
      type: "TickObject"
    }));
  }


  public get currentNode() {
    const id = executionAsyncId();
    return this.byId.get(id)!;
  }
}

type AsyncNodeArgs<CTX extends object> = {
  id: number;
  parent?: AsyncNode<CTX>;
  type: "PROMISE" | "TickObject",
  manager: AsyncNodeManager<CTX>
}

export class AsyncNode<CTX extends object> {
  constructor(public args: AsyncNodeArgs<CTX>) {
    // setters
  }

  public get id() { return this.args.id; }
  public get parent() { return this.args.parent; }

  protected _context?: CTX;
  public setContext(ctx: CTX) {
    this._context = ctx;
  }
  public getContext(): CTX | undefined {
    if (this._context) {
      return this._context;
    }
    return this.parent?.getContext();
  }

  public children: AsyncNode<CTX>[] = [];

  public drop(): number[] {
    return [
      this.id,
      this.children.map((it) => it.drop())
    ].flat(2);
  }
}