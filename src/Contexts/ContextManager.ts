import { AsyncNodeManager } from "./AsyncNodeManager";



export class ContextManager<CTX extends object> {
  public asyncManager = new AsyncNodeManager();


  public async runWithContext<OUT>(ctx: CTX, fn: () => Promise<OUT>): Promise<OUT> {
    return Promise.resolve().then(() => {
      this.asyncManager.currentNode.setContext(ctx);
      return fn();
    }) 
  }

  public getContext() {
    return this.asyncManager.currentNode.getContext();
  }
}