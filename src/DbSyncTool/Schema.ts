import { DbValueType } from "./DbValueType";

export interface ISchema {
	tableName: string;
	columns: IColumn[];
	multiColumnUniqueIndexes: Array<IMultiColumnUniqueIndex>;
}

export interface IColumn {
	name: string;
	unique: boolean;
	nullable: boolean;
	primaryKey: boolean;
	type: DbValueType;
	foreignKeys: Array<{table: string; column: string; }>;
	indexes: Array<{
		id: string;
		unique: boolean;
		primaryKey: boolean;
	}>
}

export interface IMultiColumnUniqueIndex {
	id: null | string;
	columns: string[];
}
