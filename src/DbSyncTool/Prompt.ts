import readline from "readline";


export async function promptBool(msg: string): Promise<boolean> {
	msg = msg + " (y/n): ";
	let out: boolean | undefined;
	for (let i = 0; out === undefined && i < 3; i++) {
		const resp = await promptString(msg);
		if (/^y|n$/i.test(resp) === false) {
			console.warn(`Response must be of type "y" or "n"`)
		} else {
			out = /^y$/i.test(resp);
		}
	}
	if (out === undefined) {
		throw new Error(`Could not get an appropriate response`);
	}
	return out;
}

export async function promptWarn(msg: string): Promise<void> {
	msg = "WARNING: " +msg + " (Hit enter to continue): "
	await promptString(msg);
}

export async function promptString(msg: string): Promise<string> {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});
	const out = await new Promise((resolve) => rl.question(msg, resolve));
	rl.close();
	return out as string;
}
