export type BasicDbValueType = "int" | "big_int" | "float" | "big_float" | "boolean" | "text" | "date" | "json";

// export type SpecialDbValueType = {
// 	specialType: string;
// 	closestBasic: BasicDbValueType;
// };

export type EnumDbValueType = {
	name?: string;
	enum: Array<string | number>;
};


export type DbValueType = BasicDbValueType | EnumDbValueType;
