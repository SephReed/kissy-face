import { IssueChecker } from "../IssueChecker";
import { ModColumnNullable } from "../../Fixers/ModColumnNullable.fixer";


export const MismatchNullable = new IssueChecker({
	ignore: ["mismatch-nullable"] as const,
	suggestedFixes: [
		ModColumnNullable,
	],
	checker: async (ctx) => {
		return ctx.expected.columns.filter((expect) => {
			const existingCol = ctx.findExistingColumn(expect);
			return existingCol && existingCol.nullable !== expect.nullable
		}).map((col) => ({
			column: col.name,
			msg: `Column "${col.name}" is ${col.nullable ? `"nullable"` : `"NOT NULL"`} in the schema, but `
				+ `${!col.unique ? `"nullable"` : `"NOT NULL"`} in database.`,
		}));
	},
});
