import { IssueChecker } from "../IssueChecker";
import { DropColumnFix, RenameColumnFix } from "../../Fixers";


export const ColumnOnlyInDb = new IssueChecker({
	ignore: ["exists-only-in-db"] as const,
	suggestedFixes: [
		RenameColumnFix,
		DropColumnFix
	],
	checker: async ({existing, expected, checker }) => {
		const nonSpecifiedCols = existing.columns.filter(({name}) => {
			const specified = expected.columns.some((expected) => name === expected.name);
			return !specified;
		});
		return nonSpecifiedCols.map((col) => ({
			column: col.name,
			msg: `Column "${col.name}" exists in database, but is not listed in expected schema`,
		}));
	},
});
