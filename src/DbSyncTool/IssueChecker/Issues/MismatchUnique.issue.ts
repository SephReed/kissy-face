import { IssueChecker } from "../IssueChecker";
import { MakeUniqueFixer, DropUniqueFixer } from "../../Fixers/UniqueConstraint.fixer";
import { findArrayMismatch } from "../../../Utils/findArrObjMismatch"


export const MismatchUnique = new IssueChecker({
	ignore: ["mismatch-unique"] as const,
	suggestedFixes: [
		MakeUniqueFixer,
		DropUniqueFixer,
	],
	checker: async (ctx) => {
		const { existing, expected } = ctx;
		const expectedConstraints = {
			needsCreate: [] as string[][],
			needsDrop: [] as string[][],
			noDrop: [] as string[],
		}
		const multiUniqueMatch = findArrayMismatch(
			expected.multiColumnUniqueIndexes,
			existing.multiColumnUniqueIndexes,
			(a, b) => {
				return findArrayMismatch(a.columns, b.columns) === null;
			}
		);
		if (multiUniqueMatch) {
			multiUniqueMatch.hasExtra.forEach((it) => expectedConstraints.needsCreate.push(it.columns));
			multiUniqueMatch.isMissing.forEach((it) => expectedConstraints.needsDrop.push(it.columns));
		}
		expected.columns.forEach((col) => {
			const existingCol = ctx.findExistingColumn(col);
			if (existingCol && existingCol.unique !== col.unique) {
				if (existingCol.primaryKey) {
					expectedConstraints.noDrop.push(col.name);
				} else if (existingCol.unique) {
					expectedConstraints.needsDrop.push([col.name]);
				} else {
					expectedConstraints.needsCreate.push([col.name]);
				}
			}
		})

		return [
			...expectedConstraints.needsDrop.map((cols) => ({
				column: cols.join(),
				suggestions: [DropUniqueFixer],
				msg: `Unique constraint exists on `
					+ (cols.length === 1 ? `column "${cols[0]}"` : `columns [${cols.map((it) => `"${it}"`).join(", ")}]`)
					+ ` in database but does not exist in schema.`,
			})),

			...expectedConstraints.noDrop.map((column) => ({
				column,
				suggestions: [`If you'd like to keep it as a primary key, you should instead set { unique: true } in your schema.`],
				msg: `Unique constraint exists on primary key column "${column}" in database but does not exist in schema. `
					+ `This constraint can not be dropped, without first dropping the primary key.\n`
			})),

			...expectedConstraints.needsCreate.map((cols) => ({
				column: cols.join(),
				suggestions: [MakeUniqueFixer],
				msg: `Unique constraint exists on `
					+ (cols.length === 1 ? `column "${cols[0]}"` : `columns [${cols.map((it) => `"${it}"`).join(", ")}]`)
					+ ` in schema but does not exist in database.`,
			}))
		];
		// const test = expected.multiColumnUniqueIndexes.filter((expect) => {
		// 	return existing.multiColumnUniqueIndexes.find((exists) => {

		// 	})
		// })
		// return expected.columns.filter((expect) =>
		// 	existing.columns.find((existing) => existing.name == expect.name)!.unique !== expect.unique
		// ).map((col) => ({
		// 	column: col.name,
		// 	msg: `Column "${col.name}" is ${col.unique ? `"unique"` : `"not unique"`} in the schema, but `
		// 		+ `${!col.unique ? `"unique"` : `"not unique"`} in database.`,
		// }));
	},
});
