import { IssueChecker } from "../IssueChecker";
import { AddColumnFix } from "../../Fixers/AddColumn.fixer";


export const ColumnOnlyInProject = new IssueChecker({
	ignore: ["does-not-exist-in-db"] as const,
	suggestedFixes: [
		AddColumnFix,
	],
	checker: async ({existing, expected }) => {
		const nonExistentCols = expected.columns.filter(({name}) => {
			const specified = existing.columns.some((expected) => name === expected.name);
			return !specified;
		});
		return nonExistentCols.map((col) => ({
			column: col.name,
			msg: `Column "${col.name}" exists in expected schema, but does not exist in database`,
		}));
	},
});
