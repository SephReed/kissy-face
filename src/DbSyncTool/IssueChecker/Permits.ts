

export const RenameColumnPermit = { modifyColumn: "$CURRENT_COL_NAME", renameTo: "a-known-column-name"};

export const DropColumnPermit = { dropColumn: "$CURRENT_COL_NAME", itWillBeDeleted: "I know" as const };
