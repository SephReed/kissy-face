import { Knex } from "knex";
import { IColumn, ISchema } from "../Schema";
import { Fixer } from "../Fixers/Fixer"

export interface IIssueCheckerArgs<IGN extends readonly string[], FIXS extends Fixer<any>> {
	ignore: IGN;
	suggestedFixes: FIXS[];
	checker: (ctx: IssueContext<IGN, FIXS>) => Promise<Omit<IIssueArgs, "ctx">[]>
}

export interface IProblemSpaceArgs {
	db: Knex;
	existingSchema: ISchema;
	expectedSchema: ISchema;
	permits: object[];
}

export class ProblemSpace{
	constructor(protected args: IProblemSpaceArgs){}

	public get db() { return this.args.db; }
	public get existingSchema() { return this.args.existingSchema; }
	public get expectedSchema() { return this.args.expectedSchema; }
	public get permits() { return this.args.permits; }

	public findExistingColumn(findBy: string | IColumn) {
		const name = typeof findBy === "string" ? findBy : findBy.name;
		return this.existingSchema.columns.find((it) => it.name === name);
	}
}

export interface IssueCheck {
	issues: Issue[],
	ignoredIssues: Issue[]
}

export class IssueChecker<
	IGN extends readonly string[],
	FIXS extends Fixer<any>
> {
	constructor (protected args: IIssueCheckerArgs<IGN, FIXS>){}

	public async checkForIssues(checkMe: ProblemSpace): Promise<IssueCheck> {
		const ctx = new IssueContext(checkMe, this) as IssueContext<IGN, FIXS>;
		let issues: Issue[] = [];
		let ignoredIssues: Issue[] = [];
		const issueArgs = await this.args.checker(ctx);
		issueArgs.map((args) => new Issue({...args, ctx}))
		.forEach((issue) => {
			issue.hasIgnorePermit(issue.column!) ? ignoredIssues.push(issue) : issues.push(issue);
		});
		return {
			issues,
			ignoredIssues
		};
	}

	public get ignore() { return this.args.ignore; }
	public get suggestedFixes() { return this.args.suggestedFixes; }




}


export class IssueContext<
	IGN extends readonly string[],
	FIXS extends Fixer<any>
> {
	constructor(public problemSpace: ProblemSpace, public checker: IssueChecker<IGN, FIXS>) {

	}

	public get existing() { return this.problemSpace.existingSchema; }
	public get expected() { return this.problemSpace.expectedSchema; }
	public get permits() { return this.problemSpace.permits; }

	public findExistingColumn(findBy: string | IColumn) {
		return this.problemSpace.findExistingColumn(findBy);
	}
}



export interface IIssueArgs {
	msg: string;
	column: null | string;
	ctx: IssueContext<any, any>;
	suggestions?: Array<string | Fixer<any>>;
}

export class Issue {
	constructor(protected args: IIssueArgs){}


	public get ctx() { return this.args.ctx; }
	public get msg() { return this.args.msg; }
	public get column() { return this.args.column; }
	// 	const out = this.args.column;
	// 	return Array.isArray(out) ? out.join() : out;
	// }
	public get suggestedFixes() {
		return this.args.suggestions || this.ctx.checker.suggestedFixes as Fixer<any>[]
	}

	public hasIgnorePermit(forColumn: string) {
		return this.ctx.permits.some((permit: any) => {
			permit = permit || {};
			return permit.ignoreColumn === forColumn && this.ctx.checker.ignore.includes(permit.issue);
		});
	}

	public findFixes(forColumn: string) {
		const out = this.suggestedFixes.map((fix) => {
			if (typeof fix === "string") { return; }
			const permit = this.ctx.permits.find((permit) => fix.takesPermit(permit, forColumn));
			return permit ? { permit, fix } : undefined;
		}).filter((it) => !!it);
		return out as Array<{ permit: object; fix: Fixer<any> }>
	}

	public get permitSuggestions(): string[] {
		const ignoreIssueType = this.ctx.checker.ignore.map((it: string) => `"${it}"`).join(" | ");
		return [
			...this.suggestedFixes.map((fix) => typeof fix === "string" ? fix : fix.suggestionText),
			`To ignore this issue add { ignoreColumn: "$COL_NAME", issue: ${ignoreIssueType} } to $PERMIT_LIST`
		];
	}
}
