import { Knex } from "knex";
import { DbValueType } from "../DbValueType"
import { IColumn, IMultiColumnUniqueIndex, ISchema } from "../Schema";
import { DbSyncToolClientIntegration } from "./ClientIntegration";


class _SQLite3ClientIntegration extends DbSyncToolClientIntegration {

	public disabledFeatures = {
		enums: "While SQLite3 can create enums, they are un-modifiable after creation.  This opposes the general use-case of SQLite (flexibility over performance), and as such they are disabled.",
		// await db.raw("PRAGMA foreign_keys = ON;");
		foreignKeys: "While SQLite3 can create foreign keys, they are un-modifiable after creation.  This opposes the general use-case of SQLite (flexibility over performance), and as such they are disabled.",
	}

	public async getExistingSchema(db: Knex<any, unknown[]>, tableName: string): Promise<ISchema> {
		const info = await db.raw(`PRAGMA table_info(${tableName})`) as Array<{
			cid: number,
			name: string,
			type: string,
			notnull: 0 | 1,
			dflt_value: any,
			pk: 0 | 1
		}>;
		const columns: IColumn[] = info.map((colInfo) => {
			let type: DbValueType = "text";
			switch (colInfo.type.replace(/\(.*?\)/, "").toUpperCase()) {
				case "INT":
				case "TINYINT":
				case "SMALLINT":
				case "INT2":
				case "INT8":
				case "INTEGER": type = "int"; break;

				case "MEDIUMINT":
				case "BIGINT":
				case "UNSIGNED BIG INT": type = "big_int"; break;

				case "CHARACTER":
				case "VARCHAR":
				case "VARYING CHARACTER":
				case "NCHAR":
				case "NATIVE CHARACTER":
				case "NVARCHAR":
				case "CLOB":
				case "TEXT": type = "text"; break;

				case "BLOB": break;

				case "FLOAT":
				case "REAL": type = "float"; break;

				case "DOUBLE":
				case "DECIMAL":
				case "DOUBLE PRECISION": type = "big_float"; break;

				case "BOOLEAN": type = "boolean"; break;

				case "DATE":
				case "DATETIME": type = "date"; break;

				case "NUMERIC": type = "float"; break;
				case "JSON": type = "json"; break;
			}

			return {
				name: colInfo.name,
				type,
				nullable: !colInfo.notnull,
				unique: false,
				primaryKey: !!colInfo.pk,
				indexes: [],
				foreignKeys: []
			}
		});

		// const dbDescribe = await db.raw(`select * from sqlite_master where type='table' and name='${tableName}'`);
		// const enumMatches = dbDescribe.match(/(`\w+?`)[^(),]+check \(\1 in \(([^)]+)/g);

		const indexList = await db.raw(`PRAGMA index_list(${tableName})`) as Array<{
			seq: 0,
			name: string,
			unique: 0 | 1,
			origin: 'c' | 'pk',
			partial: 0 | 1
		}>

		const multiColumnUniqueIndexes: IMultiColumnUniqueIndex[] = [];
		for (const index of indexList) {
			const indexInfo = await db.raw(`PRAGMA index_info(${index.name})`) as [{ seqno: 0, cid: 2, name: string }];
			if (indexInfo.length > 1) {
				multiColumnUniqueIndexes.push({
					id: index.name,
					columns: indexInfo.map((col) => col.name),
				});

			} else {
				const colInfo = columns.find((col) => col.name === indexInfo[0].name)!;
				colInfo.indexes.push({
					id: index.name,
					unique: !!index.unique,
					primaryKey: index.origin === "pk",
				});
				colInfo.unique = colInfo.unique || !!index.unique;
			}
		}
		return {
			tableName,
			columns,
			multiColumnUniqueIndexes
		};
	}

	public async setColumnUniqueState(args: {
		db: Knex;
		tableName: string;
		colName: string;
		unique: boolean;
		currentState: IColumn;
	}) {
		const { db, unique, currentState, tableName, colName } = args;
		if (unique) {
			const indexName = `${tableName}_${colName}_unique`;
			await db.raw(`CREATE UNIQUE INDEX ${indexName} ON ${tableName}(${colName});`);
			console.log(`Successfully created new index "${indexName}"`);
			return;
		} else {
			const uniqueIndexes = currentState.indexes.filter(({unique, primaryKey}) => unique && !primaryKey);
			await Promise.all(uniqueIndexes.map(async (index) => {
				await db.schema.table(tableName, (table) => table.dropIndex([], index.id));
				console.log(`Successfully removed unique index "${index.id}"`);
			}));
			return;
		}
	}
}



export const SQLite3ClientIntegration = new _SQLite3ClientIntegration();
