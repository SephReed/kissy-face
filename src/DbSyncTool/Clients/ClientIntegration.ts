import { Knex } from "knex";
import { DbValueType } from "../DbValueType"
import { IColumn, ISchema } from "../Schema";


// export interface ExistingColumn {
// 	name: string;
// 	unique: boolean;
// 	nullable: boolean;
// 	primaryKey: boolean;
// 	type: DbValueType;
// 	indexes: Array<{
// 		id: string;
// 		unique: boolean;
// 		primaryKey: boolean;
// 	}>
// }

// export interface IMultiColumnUniqueIndex {
// 	id: string;
// 	columns: string[];
// }

// export interface IExistingSchema {
// 	columns: ExistingColumn[],
// 	multiColumnUniqueIndexes: Array<IMultiColumnUniqueIndex>;
// }

export abstract class DbSyncToolClientIntegration {
	public abstract disabledFeatures: {
		enums?: string;
		foreignKeys?: string;
	};

	public abstract getExistingSchema(db: Knex, tablename: string): Promise<ISchema>;

	public async getExistingColumnList(db: Knex, tableName: string): Promise<IColumn[]> {
		return (await this.getExistingSchema(db, tableName)).columns;
	}

	public async getExistingColumnMap(db: Knex, tableName: string): Promise<Map<string, IColumn>> {
		const out = new Map();
		(await this.getExistingColumnList(db, tableName)).forEach((col) => out.set(col.name, col));
		return out;
	};

	public abstract setColumnUniqueState(args: {
		db: Knex;
		tableName: string;
		colName: string;
		unique: boolean;
		currentState: IColumn;
	}): Promise<void>;
}
