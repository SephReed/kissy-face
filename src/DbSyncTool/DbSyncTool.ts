import { Knex } from "knex";
import { ProblemSpace, IssueChecker } from "./IssueChecker/IssueChecker";
import { ColumnOnlyInDb, ColumnOnlyInProject, MismatchNullable, MismatchUnique } from "./IssueChecker/Issues";
import { createColumn } from "./Fixers/AddColumn.fixer"
import { IColumn, ISchema } from "./Schema";
import { promptBool, promptWarn } from "./Prompt";
import { DbSyncToolClientIntegration, SQLite3ClientIntegration } from "./Clients";
import { Fixer } from "./Fixers";


const IssueList = [
	ColumnOnlyInDb,
	ColumnOnlyInProject,
	MismatchUnique,
	MismatchNullable
] as const;

type inferIgnores<T> = T extends IssueChecker<infer O, any> ? O : never;
type inferPermits<T> = T extends IssueChecker<any, infer O> ?
	O extends Fixer<infer O2> ? O2 : never
: never;


export type IgnoreType = inferIgnores<typeof IssueList[number]>[number];
export type FixPermit = inferPermits<typeof IssueList[number]>;

export type DbSyncPermit = { ignoreColumn: string, issue: IgnoreType } | FixPermit;


export class DbSyncTool {
	// public static knownTableNames: string[] = [];

	constructor(protected args: {
		db: Knex,
		tableName: string;
		expectedSchema: ISchema,
		permits: Array<DbSyncPermit>
	}) {}

	public get clientType() {
		return this.args.db.client.config.client as string;
	}

	public async sync() {
		const { db, tableName, expectedSchema } = this.args;
		// if (this.clientType === "sqlite3") {
		// 	await db.raw("PRAGMA foreign_keys = ON;");
		// }

		// if (DbSyncTool.knownTableNames.includes(tableName)) {
		// 	throw new Error([
		// 		`There already exists a schema that was synced with the table name "${tableName}".`,
		// 		`Redundant one has columns [${expectedSchema.columns.map(({name}) => name)}]`
		// 	].join("\n"));
		// }
		// DbSyncTool.knownTableNames.push(tableName);

		if (await db.schema.hasTable(tableName) === false) {
			const allowTableCreate = await promptBool(`\nThe table "${tableName}" does not yet exist.  Would you like it to be created?`);
			if (allowTableCreate) {
				try {
					return this.createTable();
				} catch (err) {
					throw new Error(`Could not create new table "${tableName}" with columns [${expectedSchema.columns.map(({name}) => name).join()}]. Perhaps you forgot to change the name?`)
				}
			} else {
				console.warn(`Skipping creation of table "${tableName}".  This will likely create errors in your API.`)
			}
		}

		const existingSchema = await this.getClientIntegration().getExistingSchema(db, tableName);
		const problemSpace = new ProblemSpace({
			db,
			permits: this.args.permits,
			existingSchema,
			expectedSchema,
		});

		const issueSets = await Promise.all(IssueList.map((checker) => checker.checkForIssues(problemSpace)));
		let issueNum = 0;
		let unfixedIssueCount = 0;
		let notSynced = false;
		for (const { issues, ignoredIssues } of issueSets) {
			if (issues.length || ignoredIssues.length) {
				notSynced = true;
				console.warn(`Table "${tableName}" is not properly synced:`)
			}
			for (const ignoreIssue of ignoredIssues) {
				console.warn(`\x1b[33mIgnoring Issue:\x1b[0m ${ignoreIssue.msg}`)
			}
			for (const issue of issues) {
				const template = (str: string) => {
					return str.replace(/\$PERMIT_LIST/g, "syncPermits")
						.replace(/\$COL_NAME/g, issue.column || "[column name]")
				}

				issueNum++;
				console.error(`  ${issueNum}. ${issue.msg}`);

				let fixed = false;
				const fixes = issue.findFixes(issue.column!);
				if (fixes.length) {
					if (fixes.length === 1) {
						const {permit, fix} = fixes[0];
						const doFix = await promptBool(`\n\x1b[32mPermit found for fix.\x1b[0m  Would you like to ${template(fix.explain(permit as any))}`);
						if (doFix) {
							fixed = await fix.runFix({
								ctx: issue.ctx.problemSpace,
								permit: permit as any,
								column: issue.column!,
								clientIntegration: this.getClientIntegration(),
							});
							if (fixed) {
								console.log("\x1b[32mFix successful!\x1b[0m");
							} else {
								console.log("\x1b[31mFix failed...\x1b[0m");
							}
						}
					}
				}
				if (!fixed) {
					unfixedIssueCount++;
					const suggestions = issue.permitSuggestions;
					if (suggestions.length) {
						console.error([
							"  Suggestions:",
							...suggestions.map((it) =>`    - ${template(it)}`),
						].join("\n"));
					}
				}
			}
		}
		if (unfixedIssueCount > 0) {
			throw new Error(`Table "${tableName}" has unresolved syncing issues.`)
		} else if (notSynced) {
			console.log("");
		} else {
			console.log(`Table "${tableName}" is fully synced.`)
		}
	}


	public async createTable() {
		const { db, tableName, expectedSchema } = this.args;
		console.log(`Creating new table "${tableName}"...`);
		await db.schema.createTable(tableName, (table) => {
			expectedSchema.columns.forEach((col) => this.addColumn(table, col));

			expectedSchema.multiColumnUniqueIndexes.forEach((multiUnique) => {
				table.unique(multiUnique.columns);
			})
		})
		console.log(`Table "${tableName}" created!`);
	}

	protected addColumn(
		table: Knex.TableBuilder,
		columnInfo: IColumn,
	){
		return createColumn(table, columnInfo, this.getClientIntegration());
	}


	protected getClientIntegration(): DbSyncToolClientIntegration {
		switch (this.clientType) {
			case "sqlite3": return SQLite3ClientIntegration;
		}
		throw new Error(`Client integration for "${this.clientType}" not yet implemented`);
	}
}
