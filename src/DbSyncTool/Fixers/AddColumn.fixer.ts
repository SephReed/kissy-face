import { Knex } from "knex";
import { DbSyncToolClientIntegration } from "../Clients";
import { promptBool } from "../Prompt";
import { IColumn } from "../Schema";
import { Fixer } from "./Fixer";

const permitEg = { addColumn: "$COL_NAME" };

export const AddColumnFix = new Fixer({
	suggestionText: `To add the column, add $PERMIT_EG to $PERMIT_LIST`,
	permitEg,
	explain: () => `create column "$COL_NAME" based off the given schema`,
	permitMatchesColumn: "addColumn",
	runFix: async (args) => {
		const { permit, ctx, column, clientIntegration } = args;
		try {
			await ctx.db.schema.table(ctx.expectedSchema.tableName, (table) => {
				createColumn(
					table,
					ctx.expectedSchema.columns.find((col) => col.name === column)!,
					clientIntegration
				)
			});
			console.log(`Created column "${column}"`);
			return true;
		} catch(err) {
			console.error(`Unable to create column "${column}"`);
			console.error(err);
			return false;
		}
	}
})


export function createColumn(
	table: Knex.TableBuilder,
	columnInfo: IColumn,
	clientIntegration: DbSyncToolClientIntegration,
) {
	const { name, unique, nullable, primaryKey } = columnInfo;
	let { type } = columnInfo;
	let col: Knex.ColumnBuilder | undefined;
	if (typeof type === "object") {
		// if ("specialType" in type) {
		// 	type = type.specialType as any;
		// } else
		if ("enum" in type) {
			const noEnum = clientIntegration.disabledFeatures.enums;
			if (noEnum) {
				// promptWarn(noEnum);
				console.warn(`\n\x1b[31mWARNING: Ignoring enum constraint for "${name}"\x1b[0m\n` + noEnum + "\n");
				type = typeof type.enum[0] === "string" ? "text" : "int";
			} else {
				col = table.enu(name, type.enum);
			}
		}
	}
	if (!col) {
		switch (type) {
			case "boolean": col = table.boolean(name); break;
			case "int": col = table.integer(name); break;
			case "big_int": col = table.bigInteger(name); break;
			case "float": col = table.float(name); break;
			case "big_float": col = table.double(name); break;
			case "date": col = table.date(name); break;
			case "json": col = table.json(name); break;
			case "text":
			default: col = table.text(name); break;
		}
	}

	unique && (col = col.unique());
	col = nullable ? col.nullable() : col.notNullable();
	primaryKey && (col = col.primary());

	if (columnInfo.foreignKeys.length) {
		const noForeign = clientIntegration.disabledFeatures.foreignKeys;
		if (noForeign) {
			console.warn(`\n\x1b[31mWARNING: Ignoring foreign key constraint for "${name}"\x1b[0m\n` + noForeign + "\n");
		} else {
			columnInfo.foreignKeys.forEach(({table, column}) => {
				col!.references(column).inTable(table);
			})
		}
	}
}
