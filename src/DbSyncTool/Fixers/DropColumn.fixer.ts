import { promptBool } from "../Prompt";
import { Fixer } from "./Fixer";

const permitEg = { dropColumn: "$COL_NAME", itWillBeDeleted: "I know" as const };

export const DropColumnFix = new Fixer({
	suggestionText: `To drop the column, add $PERMIT_EG to $PERMIT_LIST`,
	permitEg,
	explain: () => `drop the column "$COL_NAME", permanently deleting its data`,
	permitMatchesColumn: "dropColumn",
	runFix: async (args) => {
		const { permit, ctx, column } = args;
		try {
			if (permit.itWillBeDeleted !== "I know") {
				throw new Error(`drop permit must include { itWillBeDeleted: "I know" }`);
			}
			const beSure = await promptBool(`Are you sure?  The data in column "${column}" will be gone forever.`);
			if (!beSure) {
				console.log("Drop aborted");
				return false;
			}
			await ctx.db.schema.table(ctx.expectedSchema.tableName, (table) => {
				table.dropColumn(column!);
			});
			console.log(`Dropped column "${column}"`);
			return true;
		} catch(err) {
			console.error(`Unable to drop column from "${column}"`);
			console.error(err);
			return false;
		}
	}
})
