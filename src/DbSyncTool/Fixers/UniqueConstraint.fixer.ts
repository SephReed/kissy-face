import { Fixer } from "./Fixer";


export const MakeUniqueFixer = modUniqueFixer("makeUnique");
export const DropUniqueFixer = modUniqueFixer("dropUnique");

function modUniqueFixer<T extends "makeUnique" | "dropUnique">(type: T) {
	const creation = type === "makeUnique";
	return new Fixer({
		suggestionText: `To ${creation ? "create" : "drop" } the UNIQUE constraint, add $PERMIT_EG to $PERMIT_LIST`,
		permitEg: {[type]: "$COL_NAME"},
		explain: (per) => {
			const cols = (per.makeUnique || per.dropUnique).split(",");
			return `${creation ? "create" : "drop"} UNIQUE constraint for `
				+ (cols.length > 1 ? `columns [${cols.map((it) => `"${it}"`).join(", ")}]` : `column "${cols[0]}"`)
				+ "."
		},
		permitMatchesColumn: type,
		// (colName, per) => {
		// 	console.log(colName, per);
		// 	return false;
		// },
		runFix: async (args) => {
			const { permit, ctx, column, clientIntegration } = args;
			const columns = (permit.makeUnique || permit.dropUnique).split(",");
			const creation = "makeUnique" in permit;
			try {
				if (columns.length === 1 && ctx.findExistingColumn(columns[0])?.primaryKey) {
					throw new Error(`Can not create or drop UNIQUE constraint for column "${columns[0]}" because it is a primaryKey`)
				}
				await ctx.db.schema.table(ctx.expectedSchema.tableName, (table) => {
					if (creation) {
						table.unique(columns);
					} else {
						table.dropUnique(columns);
					}
					console.log(`${creation ? "Created" : "Dropped"} UNIQUE constraint for column(s) "${columns.join(", ")}"`);
				});
				return true;
			} catch(err) {
				console.error(`Unable to modify UNIQUE constraint for column(s) "${columns.join(", ")}"`);
				console.error(err);
				return false;
			}
		}
	}) as any as Fixer<Record<T, string>>;
}
