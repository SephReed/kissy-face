import { Fixer } from "./Fixer";

const permitEg = { modColumn: "$COL_NAME", setNullable: true };

export const ModColumnNullable = new Fixer({
	suggestionText: `To change the columns nullable state, add $PERMIT_EG to $PERMIT_LIST`,
	permitEg,
	explain: ({setNullable}) => `${setNullable ? "allow" : "disallow"} NULL state from column "$COL_NAME"`,
	permitMatchesColumn: "modColumn",
	runFix: async (args) => {
		const { permit, ctx, column, clientIntegration } = args;
		throw new Error(`Not yet implemented nullable change`);
		// try {
		// 	await ctx.db.schema.table(ctx.expectedSchema.tableName, (table) => {
		// 		if (permit.setNullable) {
		// 			table.unique([column!]);
		// 		} else {
		// 			table.dropUnique([column!]);
		// 		}
		// 	});
		// 	console.log(`${permit.setUnique ? "Added state unique to" : "Removed unique state from"} column "${column}"`);
		// 	return true;
		// } catch(err) {
		// 	console.error(`Unable to modify unique state of column "${column}"`);
		// 	console.error(err);
		// 	return false;
		// }
	}
})
