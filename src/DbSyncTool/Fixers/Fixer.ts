import { KeyOf } from "../../UtilityTypes/KeyOf";
import { DbSyncToolClientIntegration } from "../Clients";
import { ProblemSpace } from "../IssueChecker/IssueChecker";

export interface RunFixArgs<PER extends object> {
	permit: PER,
	fixer: Fixer<PER>;
	ctx: ProblemSpace;
	column?: string;
	clientIntegration: DbSyncToolClientIntegration;
}


export class Fixer<PER extends object> {
	constructor(protected args: {
		permitEg: PER;
		suggestionText: string;
		runFix: (args: RunFixArgs<PER>) => Promise<boolean>;
		explain: (permit: PER) => string;
		permitMatchesColumn: KeyOf<PER> | ((colName: string, permit: PER) => boolean);
	}){}

	public get suggestionText() {
		return this.args.suggestionText.replace(/\$PERMIT_EG/, () => {
			const props = Object.entries(this.args.permitEg).map(([key, value]) => {
				const valueTxt = typeof value === "boolean" ? "true | false" : JSON.stringify(value);
				return `${key}: ${valueTxt}`
			})
			return `{ ${props.join(", ")} }`;
		});
	}

	public takesPermit(checkMe: object, forColumn?: string): checkMe is PER {
		const permitEg = this.args.permitEg;
		const knownPermit = Object.keys(permitEg).some((prop) => prop in checkMe)
			&& Object.keys(checkMe).some((prop) => prop in permitEg)

		if (!knownPermit) { return false; }

		if (forColumn === undefined) { return true; }

		const matcher = this.args.permitMatchesColumn;
		if (typeof matcher === "string") {
			return (checkMe as any)[matcher] === forColumn;
		} else {
			return matcher(forColumn, checkMe as PER);
		}
	}

	public explain(permit: PER) {
		return this.args.explain(permit);
	}

	public runFix(args: Omit<RunFixArgs<PER>, "fixer">) {
		return this.args.runFix({
			...args,
			fixer: this
		});
	}

	public getSuggestionText(args: { COL_NAME?: string }) {
		let out = this.args.suggestionText;
		Object.entries(args).forEach(([templateName, value]) => {
			out = out.replace(new RegExp(`\\$${templateName}`, "g"), value);
		});
		return out;
	}
}
