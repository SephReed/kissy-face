import { promptBool } from "../Prompt";
import { Fixer } from "./Fixer";


const permitEg = { modifyColumn: "$COL_NAME", renameTo: "a-known-column-name" };

export const RenameColumnFix = new Fixer({
	suggestionText: `To rename the column, add $PERMIT_EG to $PERMIT_LIST`,
	permitEg,
	permitMatchesColumn: "modifyColumn",
	explain: (per) => `rename the column "$COL_NAME" to "${per.renameTo}"`,
	runFix: async (args) => {
		const { permit, ctx, column } = args;
		try {
			if (ctx.expectedSchema.columns.some((col) => col.name === permit.renameTo) === false) {
				const goOn = await promptBool(`Warning: "${permit.renameTo}" is not a name of any of the columns in your expected schema.  Rename anyways?`)
				if (!goOn) {
					console.error("Aborted renaming the column");
					return false;
				}
			}
			await ctx.db.schema.table(ctx.expectedSchema.tableName, (table) => {
				table.renameColumn(column!, permit.renameTo);
			});
			console.log(`Renamed column from "${column}" to "${permit.renameTo}"`);
			return true;
		} catch(err) {
			console.error(`Unable to rename column from "${column}" to "${permit.renameTo}"`);
			console.error(err);
			return false;
		}
	}
})
