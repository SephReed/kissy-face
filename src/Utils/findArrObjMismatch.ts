export function findArrayMismatch<T>(
	target: T[],
	matchTo: T[],
	matcher: (a: T, b: T) => boolean = (a, b) => a===b
) {
	const isMissing = matchTo.slice(0);
	const hasExtra = target.slice(0).filter((itA) => {
		let foundIndex = isMissing.findIndex((itB) => matcher(itA, itB));
		if (foundIndex === -1) {
			return true;
		}
		isMissing.splice(foundIndex, 1);
		return false;
	});
	if (hasExtra.length === 0 && isMissing.length === 0) {
		return null;
	}
	return { isMissing, hasExtra };
}
