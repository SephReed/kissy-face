const useProdDataInDev = true;

export class JsonRepo<T extends { id: string; isProd: boolean }> {

	constructor(protected data: T[]) {}

	public async get(id: string): Promise<T | null> {
		return this.data.find((it) => it.id === id) || null;
	}

	public async findOne(filter: Partial<T>): Promise<T | null> {
		const out = await this.find(filter, {
			$limit: 1
		});
		return out[0] || null;
	}

	public async find(
		filter: Partial<T>,
		args: {
			$limit?: number,
			// $select?: SELECTED,
			// $sort?: {[key in keyof ALL_COLS]?: number}
		} = {}
	): Promise<T[]> {
		const where = Object.entries(filter) as Array<[keyof T, any]>;
		const out: T[] = [];
		console.log(where);
		for (const item of this.data) {
			let mismatch = false;
			for (const [key, value] of where) {
				if (item[key] !== value) {
					mismatch = true;
					break;
				}
			}
			if (mismatch === false) {
				out.push(item);
				if (args.$limit && args.$limit <= out.length) {
					break;
				}
			}
		}
		return out;
	}

	public rawData() {
		return this.data;
	}




	// create(entity: DataObject<T>, options?: Options): Promise<T>;
	// createAll(entities: DataObject<T>[], options?: Options): Promise<T[]>;
	// save(entity: T, options?: Options): Promise<T>;
	// find(filter?: Filter<T>, options?: Options): Promise<(T & Relations)[]>;
	// update(entity: T, options?: Options): Promise<void>;
	// delete(entity: T, options?: Options): Promise<void>;
	// updateAll(data: DataObject<T>, where?: Where<T>, options?: Options): Promise<Count>;
	// updateById(id: ID, data: DataObject<T>, options?: Options): Promise<void>;
	// replaceById(id: ID, data: DataObject<T>, options?: Options): Promise<void>;
	// deleteAll(where?: Where<T>, options?: Options): Promise<Count>;
	// deleteById(id: ID, options?: Options): Promise<void>;
	// count(where?: Where<T>, options?: Options): Promise<Count>;
	// exists(id: ID, options?: Options): Promise<boolean>;
	// execute(command: Command, parameters: NamedParameters | PositionalParameters, options?: Options): Promise<AnyObject>;
}
