
export function getInequalityMessage<T extends object>(former: T,
	latter: T,
	args: {
		quick?: boolean;
		ignore?: Array<keyof T>
	} = {}
) {
	const check = notEqualShallow(former, latter, args);
	if (check === true) {
		return "Not equal";
	} else if (check === false) {
		return undefined;
	}
	return check;
}



// string or true means not equal
// false means equal
// done so returning string does not end up being truthy
function notEqualShallow<T extends object>(
	former: T,
	latter: T,
	args: {
		quick?: boolean;
		ignore?: Array<keyof T>
	} = {}
): boolean | string {
	const oldKeys = Array.from(Object.keys(former));
	const newKeyMap = {
		...latter
	};

	const missingLatter = [] as string[];
	const mismatch = [] as string[];
	const quick = args.quick || false;

	function checkKey(key: string) {
		if (args.ignore && args.ignore.includes(key as keyof T)) {
			return true;
		}
		if (key in newKeyMap === false) {
			!quick && missingLatter.push(key)
			return false;
		}
		const formerValue = former[key];
		const latterValue = newKeyMap[key];
		delete newKeyMap[key];

		if (formerValue !== latterValue) {
			!quick && mismatch.push(key);
			return false;
		}

		return true;
	}

	if (quick) {
		const oldKeyCheck = oldKeys.find((key) => !checkKey(key));
		if (oldKeyCheck) { return true; }

	} else {
		oldKeys.forEach(checkKey);
	}


	const missingFormer = Array.from(Object.keys(newKeyMap)).filter((key) => {
		if (!args.ignore) { return true; }
		return !args.ignore.includes(key as any);
	});

	if (!missingFormer.length && !missingFormer.length && !mismatch.length) {
		return false;

	} else if (quick) {
		return true;
	}

	return `Not equal. Has: ` +
		[
			mismatch.length && `non equal props (${mismatch.join(" ")})`,
			missingLatter.length && `missing props (${missingLatter.join(" ")})`,
			missingFormer.length && `props that did not exist before (${missingFormer.join(" ")})`,
		]
		.filter((it) => !!it)
		.map((txt, index, arr) => {
			if (arr.length > 1 && index === arr.length - 1) {
				return "and " + txt;
			}
			return txt;
		})
		.join(", ");
}
