
export function isApiError(checkMe: any): checkMe is IApiError {
	return checkMe && (typeof checkMe === "object") && ("status" in checkMe) && ("message" in checkMe);
}

export interface IApiError {
	status: number;
	message: string;
	details?: any;
}

export function apiErr(msg: string, details?: any): IApiError;
export function apiErr(status: number, msg: string, details?: any): IApiError;
export function apiErr(statusOrMsg: number | string, msg?: string, details?: any): IApiError {
	let err: IApiError;
	if (typeof statusOrMsg === "number") {
		err = {
			status: statusOrMsg,
			message: msg!,
			details,
		}
	} else {
		err = {
			status: 400,
			message: statusOrMsg,
			details,
		}
	}
	// console.error(err);
	return err;
}
