
export const PrimitivePropTypeList = ["string", "boolean", "number"] as const;
export type PrimitivePropType = typeof PrimitivePropTypeList[number];


export type SingleValidatorPropType = PrimitivePropType | Validator<any>;
export type ValidatorPropType = SingleValidatorPropType | SingleValidatorPropType[];


export type ObjectValidatorArgs<TYPE extends object> = Record<keyof TYPE, ValidatorPropType>

export type ArrayValidatorArgs = SingleValidatorPropType[];

export type ValidatorArgs<TYPE extends object> = ObjectValidatorArgs<TYPE> | ArrayValidatorArgs;


export type ValidationBadPropType<TYPE> = {
	badType: {
		propName: keyof TYPE,
		expected: PrimitivePropType,
		got: string,
	}
}
export type ValidationFailureReason<TYPE> = { requires: keyof TYPE}  | ValidationBadPropType<TYPE>

export type VerboseValidation<TYPE> = {
	valid: boolean;
	errors?: string | Array<ValidationFailureReason<TYPE>>
}



export class Validator<TYPE extends object> {
	public static fromExample<TYPE extends object>(obj: TYPE) {
		return new Validator<TYPE>(Validator.getValitadorArgsFromExample(obj));
	}

	public static getValitadorArgsFromExample<TYPE extends object>(obj: TYPE): ValidatorArgs<TYPE> {
		if ( Array.isArray(obj)) { throw "Array validation not yet implemented"; }

		const valArgs: ObjectValidatorArgs<TYPE> = {} as any;
		Array.from(Object.keys(obj)).forEach((key) => {
			const exampleValue = obj[key];
			let type = typeof exampleValue;
			if (PrimitivePropTypeList.includes(type as any)) {
				return valArgs[key] = type;
			}

			if (type === "object") {
				throw "TODO";
			}
		});

		return valArgs;
	}

	protected type: "object" | "array";
	protected defaultRequired: boolean;
	protected doesRequireProp: Map<keyof TYPE, boolean>;
	protected propValidations: ValidatorArgs<TYPE>;

	public constructor(propValidations: ValidatorArgs<TYPE>) {
		this.propValidations = propValidations;
		this.type = Array.isArray(propValidations) ? "array" : "object";
		if (this.type === "object") {
			this.doesRequireProp = new Map();
		}
	}

	public dontRequire(props: Array<keyof TYPE>) {
		if (this.defaultRequired === undefined) {
			this.defaultRequired = true;
		}
		props.forEach((propName) => this.doesRequireProp.set(propName, false));
		return this;
	}

	public require(props: Array<keyof TYPE>) {
		if (this.defaultRequired === undefined) {
			this.defaultRequired = false;
		}
		props.forEach((propName) => this.doesRequireProp.set(propName, true));
		return this;
	}

	public validate(checkMe: object) {
		const ast = this.getValidationAst(checkMe, { shallow: true});
		return ast.valid;
	}

	public verboseValidate(checkMe: object): { valid: boolean, error?: string } {
		const ast = this.getValidationAst(checkMe);
		if (ast.valid) { return ast as any; }

		const errors = ast.errors;
		if (typeof errors === "string") {
			return {
				valid: false,
				error: errors,
			}
		}

		const missing: string[] = [];
		const badType: ValidationBadPropType<TYPE>[] = [];

		errors.forEach((err) => {
			if ("badType" in err) {
				badType.push(err);
			} else {
				missing.push(err.requires as string);
			}
		});

		let error = "";
		if (missing.length) {
			const propOrProps = missing.length > 1 ? 'props' : 'prop'
			error += `Missing ${propOrProps}: ${missing.map((it) => `'${it}'`).join(', ')}`;
		}
		if (badType.length) {
			if (error.length) { error += "\n"; }
			const propOrProps = badType.length > 1 ? 'props' : 'prop'
			error += `Wrongly typed ${propOrProps}: `;
			error += badType.map(({badType}) => {
				return `'${badType.propName}' was ${badType.got} instead of ${badType.expected}`;
			}).join(", ");
		}

		return {
			valid: false,
			error
		};
	}

	public getValidationAst(
		checkMe: object,
		args: { shallow?: boolean} = {}
	): VerboseValidation<TYPE> {
		const shallow = !!args.shallow;

		if (this.type === "object") {
			if (Array.isArray(checkMe)) {
				return {
					valid: false,
					errors: "Type must be Object, got Array",
				};
			}
			const propValidations = this.propValidations as ObjectValidatorArgs<TYPE>;
			const propNames = Array.from(Object.keys(propValidations)) as Array<keyof TYPE>;

			let failReasons: ValidationFailureReason<TYPE>[] = [];

			for (const propName of propNames) {
				const isRequired = this.doesRequireProp.has(propName) ? this.doesRequireProp.get(propName) : !!this.defaultRequired;
				const checkValue = (checkMe as any)[propName];
				if (!checkValue && isRequired) {
					if (shallow) { return { valid: false }}
					failReasons.push({ requires: propName });

				} else {
					const propValidation = this.validatePropMatching(propName, checkValue);
					if (propValidation !== true) {
						if (shallow) { return { valid: false }}
						failReasons.push(propValidation);
					}
				}
			}

			failReasons = failReasons.filter((it) => !!it);

			if (failReasons.length == 0) {
				return { valid: true };
			} else {
				return {
					valid: false,
					errors: failReasons,
				}
			}
		}

		throw "TODO";
	}




	protected validatePropMatching(propName: keyof TYPE, value: any): true | ValidationBadPropType<TYPE> {
		const valid = (this.propValidations as any)[propName];
		const valueType = typeof value;
		if (PrimitivePropTypeList.includes(valueType as any)) {
			let passesValidation;
			if (Array.isArray(valid)) {
				passesValidation = valid.includes(valueType as any);
			} else {
				passesValidation = (valid === valueType);
			}
			if (passesValidation) { return true; }
			return {
				badType: {
					propName,
					expected: valid,
					got: valueType
				}
			}
		}

		throw "TODO";
	}
}
