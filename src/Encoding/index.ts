import * as Encode from "./Encode";
export { NumberEncoding } from "./Encode";

import * as Cipher from "./Cipher";
export { CipherArgs, CipherType } from "./Cipher";

import * as Hash from "./Hash";

import * as Jwt from "./Jwt";

export const Encoding = {
	...Encode,
	...Cipher,
	...Hash,
	...Jwt
};
