import * as bcrypt from 'bcrypt';

export const hash = {
	create(
		hashMe: string, 
		nonAlphaNum: "./" | "-_" = "-_",
		salt = bcrypt.genSaltSync(10),
	) {
		const out = bcrypt.hashSync(hashMe, salt);
		if (nonAlphaNum === "-_") {
			return out.replace(/\./g, "-").replace(/\//g, "_");
		}
		return out;
	},

	validate(unHashed: string, hash: string) {
		hash = hash.replace(/\-/g, ".").replace(/\_/g, "/");
		return bcrypt.compareSync(unHashed, hash);
	}
}
