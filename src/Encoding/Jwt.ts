import * as _JWT from "jsonwebtoken";

export class JwtTool {
	protected secret?: string;
	public setSecret(_secret: string) {
		this.secret = _secret;
	}

	protected demandSecret() {
		if (!this.secret) {
			throw new Error(`You must use "setSecret('your-secret-password')" before usage of JWT tool.`)
		}
		return this.secret;
	}

	public signPayload(payload: string | object | Buffer) {
		return _JWT.sign(payload, this.demandSecret())
	}

	public verifyToken<T extends string | object | Buffer>(token: string): T {
		return _JWT.verify(token, this.demandSecret()) as any;
	}
}

export const jwt = new JwtTool();


// jwt.setSecret("jwt-secret");
// const token = jwt.signPayload("test");
// console.log(token);
// const payload = jwt.verifyToken(token);
// console.log(payload);
