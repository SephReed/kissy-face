// !!! MAKE SURE TO UPDATE NODE JS !!!
import crypto from "crypto";

export type CipherType = "aes-128-gcm" | "aes-128-ccm" | "aes-192-gcm" | "aes-192-ccm" | "aes-256-gcm" | "aes-256-ccm";

export interface CipherArgs {
	key: string | undefined,
	type: CipherType,
	numAuthTagBytes?: number,
	numIvBytes?: number,
	stringBase?: "base64",
}


export class CipherTool {
	public static generateKey(cipherType: CipherType) {
		let numBytes: number;
		switch (cipherType) {
			case "aes-128-gcm": numBytes = 128 / 8; break;
			default: throw new Error(`TODO: support cipherType "${cipherType}"`);
		}
		return crypto.randomBytes(numBytes).toString("base64");
	}

	public static convertToKey(convertMe: string, cipherType: CipherType) {
		const exampleKey = CipherTool.generateKey(cipherType);
		if (exampleKey.length === convertMe.length) { return convertMe; }

		let padding = 0;
		const targetLength = exampleKey.replace(/=+/, (match) => {
			padding = match.length;
			return "";
		}).length;

		while (convertMe.length < targetLength) {
			convertMe += convertMe;
		}
		let out = convertMe.slice(0, targetLength);
		for (let i = 0; i < padding; i++) {
			out += "=";
		}
		return out;
	}

	private config: Required<CipherArgs>;

	constructor(_config: CipherArgs) {


		this.config = _config as any;
		if (_config.key) {
			this.setKey(_config.key);
		}

		this.config.numAuthTagBytes = _config.numAuthTagBytes || 16;
		this.config.numIvBytes = _config.numIvBytes || 12;
		this.config.stringBase = _config.stringBase || "base64";
		if (this.config.numAuthTagBytes < 16) { console.warn(`Be careful of short auth tags, minimum 16 recommended`); }
		if (this.config.numIvBytes < 12) { console.warn(`Be careful of short ivs, minimum 12 recommended`); }
	}

	public setKey(key: string) {
		this.config.key = CipherTool.convertToKey(key, this.config.type);
	}

	private get key() {
		const key = this.config.key;
		if (!key) {
			throw new Error(`Key must be set for Cipher before usage.  Use "cipher.setKey('my-secret-key')".`)
		}
		return key;
	}

	public encrypt(msg: string) {
		const {type, numIvBytes, numAuthTagBytes, stringBase} = this.config;
		const iv = crypto.randomBytes(numIvBytes);
		const cipher = crypto.createCipheriv(
			type,
			Buffer.from(this.key, stringBase),
			iv,
			numAuthTagBytes ? { 'authTagLength': numAuthTagBytes } as any : undefined
		);

		return [
			iv.toString(stringBase),
			cipher.update(msg, "utf8", stringBase),
			cipher.final(stringBase),
			(cipher as any).getAuthTag().toString(stringBase)
		].join("");
	}


	public decrypt(cipherText: string) {
		const {type, numIvBytes, numAuthTagBytes, stringBase} = this.config;
		let authTagCharLength: number = this.bytesToChars(numAuthTagBytes);
		let ivCharLength: number = this.bytesToChars(numIvBytes);

		const authTag = Buffer.from(cipherText.slice(-authTagCharLength), stringBase);
		const iv = Buffer.from(cipherText.slice(0, ivCharLength), stringBase);
		const encryptedMessage = Buffer.from(cipherText.slice(ivCharLength, -authTagCharLength), stringBase);

		const decipher = crypto.createDecipheriv(
			type,
			Buffer.from(this.key, stringBase),
			iv,
			{ 'authTagLength': numAuthTagBytes } as any
		);
		(decipher as any).setAuthTag(authTag);

		return [
			decipher.update(encryptedMessage + "", stringBase, "utf8"),
			decipher.final()
		].join("");
	}

	private bytesToChars(numBytes: number) {
		if (this.config.stringBase === "base64") {
			switch (numBytes) {
				case 16: return 24;
				case 12: return 16;
				case 8: return 12;
				case 4: return 8;
				case 0: return 0;
				default: throw new Error("What's the math here?");
			}
		} else {
			throw new Error("TODO: support other string types");
		}
	}
}

export const cipher = new CipherTool({
	key: undefined,
	type: "aes-128-gcm",
});



// 16:22+2
// 12:16
// 8:11+1
// 4:6+2

// ----------------------- Usage -----------------

// const keyIn = convertToCipherKey("aes-128-gcm", "secret");
// const cipher = new Cipher(keyIn, {
// 	type: "aes-128-gcm",
// 	numAuthTagBytes: 4,
// 	numIvBytes: 4,
// });
// const encrypted = cipher.encrypt("This is some string to encrypt");
// console.log(encrypted + "");
// console.log(cipher.decrypt(encrypted));





// // ------------ TEST ----------
// const keyIn = convertToCipherKey("aes-128-gcm", "secret");
// const cipher = new Cipher(keyIn, {
// 	type: "aes-128-gcm",
// 	numAuthTagBytes: 4,
// 	numIvBytes: 4,
// });
// for (let i = 0; i < 1000; i++) {
// 	const value = "asgn_" + Math.random() + "";
// 	const encrypted = cipher.encrypt(value);
// 	const decrypted = cipher.decrypt(encrypted);
// 	console.log(encrypted);
// 	if (value !== decrypted) {
// 		console.error(`mismatch between ${value} and ${decrypted}`);
// 	}
// }


// const key = Cipher.createKey("aes-128-gcm");
// const sameKey = Cipher.createKey("aes-128-gcm", key);

// console.log(key, sameKey);
