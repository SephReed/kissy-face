

const ENDEX = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";

export type NumberEncoding = "chars" | "alphanumeric" | "base-16" | "base-32" | "base-64";


export function encodeNumber(encodeMe: number, encoding: NumberEncoding) {
	if (
		isNaN(Number(encodeMe))
		|| encodeMe === null
		|| encodeMe === Number.POSITIVE_INFINITY
	) {
		throw "The input is not valid";
	} else if (encodeMe < 0) {
		throw "Can't represent negative numbers now";
	}

	if (encoding === "base-16" || encoding === "base-32") {
		return encodeMe.toString(encoding === "base-16" ? 16 : 32);
	}

	const { baseCharIndex, charCodeRange } = getCharCodeRange(encoding);

	let rixit: number; // like 'digit', only in some non-decimal radix
	let residual = Math.floor(encodeMe);
	let out = "";
	while (true) {
		rixit = residual % charCodeRange
		out = ENDEX.charAt(rixit + baseCharIndex) + out;
		residual = Math.floor(residual / charCodeRange);
		if (residual === 0) { break; }
	}

	return out;
}

export function decodeToNumber(decodeMe: string, encoding: NumberEncoding) {
	const { baseCharIndex, charCodeRange } = getCharCodeRange(encoding);
	let result = 0;
	decodeMe.split('').forEach((char) =>
		result = (result * charCodeRange) + (ENDEX.indexOf(char) - baseCharIndex)
	);
	return result;

}

function getCharCodeRange(encoding: NumberEncoding) {
	let baseCharIndex = 0;
	let charCodeRange: number;

	switch(encoding) {
		case "base-16": charCodeRange = 16; break;
		case "base-32": charCodeRange = 32; break;
		case "base-64": charCodeRange = 64; break;
		case "chars": baseCharIndex = 10; charCodeRange = 26 * 2; break;
		case "alphanumeric": charCodeRange = 62; break;
	}

	return { baseCharIndex, charCodeRange };
}




export function getRandomString(
	type: NumberEncoding,
	security: { length: number } | { bitsEntropy: number | "weak" | "decent" | "strong" | "very-strong" },
) {
	let length: number;
	if ("length" in security) {
		length = security.length;

	} else {
		let charSize: number;
		switch (type) {
			case "chars": charSize = 2 * 26; break;
			case "alphanumeric": charSize = 62; break;
			case "base-16": charSize = 16; break;
			case "base-32": charSize = 32; break;
			case "base-64": charSize = 64; break;
		}

		let targetBits: number;
		switch(security.bitsEntropy) {
			case "weak": targetBits = 32; break;
			case "decent": targetBits = 48; break;
			case "strong": targetBits = 96; break;
			case "very-strong": targetBits = 128; break;
			default: targetBits = security.bitsEntropy;
		}

		length = targetBits / (Math.log(charSize)/Math.log(2));
	}

	let out = "";
	while (out.length < length) {
		const randNum = ~~(Math.random() * 10000000);
		out += encodeNumber(randNum, type);
	}
	return out.slice(0, length);
}
