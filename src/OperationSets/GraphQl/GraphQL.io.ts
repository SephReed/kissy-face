import { GraphQLEnumType, GraphQLFieldConfig, GraphQLObjectType, GraphQLScalarType } from "graphql/type/definition";
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { PropValueType } from "../Base/Base.io";



export type GraphQlConf<
	T extends object,
	KEY extends string = KeyOf<T>
> = {
	graphQlSpecificId?: string;
	props: {
		[key in KEY]: {
			valueType: PropValueType;
			graphQlValueType?: GraphQLScalarType | GraphQLEnumType;
		}
	}
}




// ------------------------------------


export abstract class GraphQlOps {
	public abstract getGraphQlObject(): GraphQLObjectType;
	public abstract genOpenQuery(): GraphQLFieldConfig<any, any>;
	// createGqlExplorerHTML(gqlEndpoint: string): string;
	public abstract _protected: {
		genAsGQLSubSchemaField(
			keyThereAndHere: [string, string],
			listResponse: boolean,
		): GraphQLFieldConfig<any, any>
	}
}
