import { GraphQLScalarType } from "graphql/type/definition";

export const GraphQLJson = new GraphQLScalarType({
	name: "JsonData",
	serialize: (value) => value,
	// serialize: (valueFromDb) => JSON.stringify(valueFromDb),
	// parseValue: (valueFromUser) => JSON.stringify(valueFromUser),
});
