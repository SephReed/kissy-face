import { GraphQLEnumType, GraphQLEnumValueConfigMap, GraphQLFieldConfig, GraphQLFieldConfigMap, GraphQLList, GraphQLObjectType, GraphQLResolveInfo, GraphQLScalarType } from "graphql/type/definition";
import { GraphQLBoolean, GraphQLFloat, GraphQLInt, GraphQLString } from "graphql/type/scalars";
import GraphQLDate from "graphql-date";
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { CONF_ARG, PRE_OPS_ARG, SchemaOpsFactory } from "../KissyFace";
import { RepoToolsOps } from "../RepoTools/RepoTools.io";
import { GraphQlConf, GraphQlOps } from "./GraphQL.io";
import { GraphQLJson } from "./GraphQlTypes";



export function addGraphQL<
	T extends object = { "Generic is missing from addGraphQL<T>()": true },
>(): SchemaOpsFactory<
	GraphQlConf<T>,
	GraphQlOps
> {
	type PRE_CONF = GraphQlConf<T, string>;
	type PRE_OPS = RepoToolsOps<T, T>;

	return (_conf, currentOps: PRE_OPS_ARG<PRE_OPS, PRE_CONF>) => {
		const conf = _conf as any as CONF_ARG<PRE_CONF>;
		const { forEachPropConfig } = currentOps!;

		/*private*/ function demandRepo() {
			if ("repo" in currentOps! === false) {
				throw new Error(`Service is not defined, schema chain must include "addRepo()" to use this function`);
			}
			return currentOps!.repo;
		}


		/*private*/ const gqlEnumMap = new Map<string, GraphQLEnumType>();

		/*private*/ function getGqlType(
			propName: string,
			propConfig: (typeof conf)["props"][KeyOf<T>]
		): GraphQLScalarType | GraphQLEnumType {
			if ("graphQlValueType" in propConfig) {
				return propConfig.graphQlValueType! as GraphQLScalarType | GraphQLEnumType;
			}

			let valueType = propConfig.valueType;
			if (typeof valueType === "object" && "foreignKey" in valueType) {
				const foreignKey = valueType.foreignKey()
				const { schema, propName } = Array.isArray(foreignKey) ? foreignKey[0] : foreignKey;
				valueType = schema.getPropConfig(propName).valueType;
			}

			if (typeof valueType === "object") {
				if ("closestBasic" in valueType) {
					valueType = valueType.closestBasic;

				} else if ("foreignKey" in valueType) {
					throw new Error(`Foreign key for "${propName}" is also a foreignKey`);

				} else {
					let name = valueType.name || `${conf.graphQlSpecificId || conf.dbTableName}__${propName}__enum`;
					name = name.replace(/^[_a-zA-Z0-9]/g, "_");
					if (gqlEnumMap.has(name)) {
						return gqlEnumMap.get(name)!;
					}
					const values: GraphQLEnumValueConfigMap = {};
					valueType.enum.forEach((value) => {
						const gqlName = String(value).replace(/\-/g, "_");
						values[gqlName] = {
							value,
						}
					});
					const gqlEnum = new GraphQLEnumType({name, values});
					gqlEnumMap.set(name, gqlEnum);
					return gqlEnum;
				}
			}

			switch(valueType) {
				case "date": return GraphQLDate as any;
				case "int":	return GraphQLInt;
				case "big_int":
				case "float":
				case "big_float": return GraphQLFloat;
				case "text": return GraphQLString;
				case "boolean": return GraphQLBoolean;
				case "json": return GraphQLJson;
			}
		}

		/*private*/ let _graphQlObject: GraphQLObjectType;
		/*public*/ function getGraphQlObject() {
			return _graphQlObject || (
				_graphQlObject = new GraphQLObjectType({
					name: conf.graphQlSpecificId || conf.dbTableName,
					fields: () => {
						const fields = {
							...getSchemaRelationFields(),
						};
						forEachPropConfig((propName, propConfig) => {
							(fields as any)[propName as any] = {
								type: getGqlType(propName as string, propConfig as any)
							};
						});
						return fields as any;
					},
				})
			)
		}

		/*private*/ function isGqlOps(testMe: any): testMe is GraphQlOps {
			return testMe && ("getGraphQlObject" in testMe);
		}

		/*private*/ let _schemaRelationFields: GraphQLFieldConfigMap<any, any>;
		/*public*/ function getSchemaRelationFields() {
			if (!_schemaRelationFields) {
				_schemaRelationFields = {};


				if (conf.relations) {
					const schemaRelationTypes = conf.relations();
					Object.entries(schemaRelationTypes).forEach(([relationType, schemaRelations]) => {
						Object.entries(schemaRelations).forEach(([fieldName, relation]) => {
							const listResponse = relationType === "hasMany";
							const { joinFrom, joinTo } = relation;
							const schemaOps = joinTo.schema;
							if (isGqlOps(schemaOps)) {
								const fieldConfig = schemaOps._protected.genAsGQLSubSchemaField([joinFrom || conf.primaryKey, joinTo.propName] as any, listResponse);
								_schemaRelationFields[fieldName] = fieldConfig;
							} else {
								throw new Error(`Relation "${fieldName}" does not have graph ql operations`);
							}
						})
					})
				}

				forEachPropConfig((propNameHere, propConf) => {
					let { valueType } = propConf;
					if (typeof valueType === "object" && "foreignKey" in valueType) {
						let foreignKeys = valueType.foreignKey();
						foreignKeys = Array.isArray(foreignKeys) ? foreignKeys : [foreignKeys];
						foreignKeys.forEach(({ schema, propName, relationName }) => {
							if (isGqlOps(schema)) {
								const fieldConfig = schema._protected.genAsGQLSubSchemaField([propNameHere, propName], false);
								_schemaRelationFields[relationName || schema._protected.config.dbTableName] = fieldConfig;
							} else {
								throw new Error(`Relation "${propNameHere}" does not have graph ql operations`);
							}
						})
					}
				})
			}
			return _schemaRelationFields;
		}

		/*private*/ function getSelectedFieldsFromResolveInfo(
			resolveInfo: GraphQLResolveInfo
		): KeyOf<T>[] | undefined {
			const out = new Map<string, void>();
			resolveInfo.fieldNodes.forEach((it) => {
				it.selectionSet?.selections.forEach((sel) => {
					if (sel.kind === "Field") {
						const propName = sel.name.value;
						if (propName in conf.props) {
							out.set(propName);
						}
					}
				})
			})
			if (out.size === 0) { return undefined; }
			// primary key is just a gimme
			conf.primaryKey && out.set(conf.primaryKey);
			// any foreign key objects should be included because they may be used for relations
			forEachPropConfig((propName, { valueType }) => {
				if (typeof valueType === "object" && "foreignKey" in valueType) {
					out.set(propName);
				}
			});
			// all keys used explicitly by relations
			conf.relations && Object.values(conf.relations()).forEach((relateType) => {
				relateType && Object.values(relateType).forEach(({ joinFrom }) => joinFrom && out.set(joinFrom))
			})
			return Array.from(out.keys()) as any;
		}

		/*public*/ function genAsGQLSubSchemaField(
			keyThereAndHere: [string, string],
			listResponse: boolean = true
		): GraphQLFieldConfig<any, any> {
			// let sortColumn = this.schemaConfig.timeSortColumnName;
			// if (!sortColumn) {
			// 	if ("createdAt" in this.schemaConfig.props) {
			// 		sortColumn = "createdAt" as any;
			// 	}
			// }

			const out = genOpenQuery();
			if (!listResponse) { out.type = getGraphQlObject(); }
			delete out!.args![keyThereAndHere[1]];
			out.resolve = async (parent, queryFields, ctx, resolveInfo) => {
				let limit: number | undefined = undefined;
				if (queryFields._limit) {
					limit = queryFields._limit;
					delete queryFields["_limit"];
				}

				const service = demandRepo();

				const select = getSelectedFieldsFromResolveInfo(resolveInfo);
				const response = await service.find({
					...queryFields,
					[keyThereAndHere[1]]: parent[keyThereAndHere[0]]
				} as any,{ limit, select });
				return listResponse ? response : response[0];
			}
			return out;
		}

		// /*public*/ function createGqlExplorerHTML(gqlEndpoint: string) {
		// 	const rawText = readFileSync(PathTool.join(__dirname, "./Graphiql.html")) + "";
		// 	return rawText.replace(`/*{gqlEndpoint}*/`, gqlEndpoint);
		// };

		/*public*/ function genOpenQuery(): GraphQLFieldConfig<any, any> {
			const service = demandRepo();
			// let sortColumn = opsConfig.timeSortColumnName;
			// if (!sortColumn) {
			// 	if ("createdAt" in opsConfig.props) {
			// 		sortColumn = "createdAt" as any;
			// 	}
			// }

			const args = {
				_limit: { type: GraphQLInt },
			} as any;

			// if (sortColumn) {
			// 	args._sortBy = { type: GraphQlSortBy };
			// }

			forEachPropConfig((propName, propConfig) => {
				// if (propConfig.unique) { return; } // TODO: think about whether unique belongs in select
				args[propName] = { type: getGqlType(propName as string, propConfig as any) }
			});

			return {
				type: new GraphQLList(getGraphQlObject()),
				args,
				resolve: async (_: any, query: any, context, resolveInfo) => {
					const limit = query._limit || 150;
					delete query._limit;



					// if (sortColumn) {
					// 	const sortBy: "NEWEST" | "OLDEST" = query._sortBy || "NEWEST";
					// 	delete query._sortBy;
					// 	query.$sort = { [sortColumn]: sortBy === "NEWEST" ? -1 : 1 }
					// }

					const select = getSelectedFieldsFromResolveInfo(resolveInfo);
					return service.find(query, { limit, select });
				}
			}
		}

		return {
			getGraphQlObject,
			genOpenQuery,
			// createGqlExplorerHTML
			_protected: {
				genAsGQLSubSchemaField,
			}
		}
	};
}
