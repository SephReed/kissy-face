import { Knex } from "knex";
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { NonNullableKeys, NullableKeys } from "../../UtilityTypes/Nullable";
import { RequiredKeys } from "../../UtilityTypes/RequiredKeys";
import { SubpropertyMerge } from "../../UtilityTypes/SubpropertyMerge";
import { DeepIntersectUnion } from "../../UtilityTypes/UnionToIntersection";



export type BasicDbValueType = "int" | "big_int" | "float" | "big_float" | "boolean" | "text" | "date" | "json";

export type SpecialDbValueType = {
	specialType: string;
	closestBasic: BasicDbValueType;
};

export type EnumDbValueType = {
	name?: string;
	enum: Array<string | number>;
};

export type ForeignKey = {
	schema: BaseOps<any, any>;
	propName: string;
	relationName?: string;
}
export type ForeignKeyValueType = {
	foreignKey: () => ForeignKey | ForeignKey[]
}

export type PropValueType = BasicDbValueType | SpecialDbValueType | EnumDbValueType | ForeignKeyValueType;

interface ISchemaRelation<T extends object> {
	joinFrom?: KeyOf<T>;
	joinTo: ForeignKey;
}
export interface ISchemaRelationFields<T extends object> { [key: string]: ISchemaRelation<T> };

type B = "a" extends "a" | "b" ? true : false;


export type BaseConf<
	T extends object,
	ARGS extends Partial<T>
> = {
	dbTableName: string;
	primaryKey: null | KeyOf<T>;
	timeSortColumnName?: KeyOf<T>;
	relations?: () => {
		hasMany?: ISchemaRelationFields<T>,
		belongsTo?: ISchemaRelationFields<T>,
		oneToOne?: ISchemaRelationFields<T>,
	},
	multiColumnUniqueIndexes?: Array<KeyOf<T>[]>,
	props: SubpropertyMerge<(
		{ [key in KeyOf<T>]: {
			dbColName?: string;
			// expunge?: true;
			unique?: true;
			// public?: true;
			valueType?: PropValueType;
			converter?: {
				fromDbType: (arg: any) => any;
				toDbType: (arg: any) => any;
			},
			nullable: (key extends NullableKeys<T> ? true : false)
		}}

		& { [key in RequiredKeys<ARGS>]: {
			mustBeSpecified: true;
		}}
	)>

		// & { [key in Exclude<RequiredKeys<T>, RequiredKeys<ARGS>>]: {
		// 	creationFill(givenValue: any): any | Promise<any>;
		// }}



	// creationEventArgs?: {
	// 	type: EventType;
	// 	eventForeignIdName: keyof IEvent;
	// };
	// additionalCreateArgs: SchemaPropsConfigs<ADDITIONAL_CREATE_ARGS, ADDITIONAL_CREATE_ARGS, ADDITIONAL_CREATE_ARGS>
	// hooks?: Partial<HooksObject>;
	// hooks?: Partial<IHookOptions>;
	// checkItemAuth: ItemAuthCheck<TABLE_COLS>
	// auth?: ItemAuthCheck<TABLE_COLS> | {
	// 	read: ItemAuthCheck<TABLE_COLS>,
	// 	write: ItemAuthCheck<TABLE_COLS>
	// }
} 



// ----------------------------------------



export type InferProps<CONF> = CONF extends { props: any } ? CONF["props"] : {};
type TypeFrom<T> = T extends object ? DeepIntersectUnion<T[keyof T]> : never;
export type InferAllPropsType<CONF> = TypeFrom<InferProps<CONF>>;
export type InferPropType<CONF, KEY extends string> = KEY extends keyof InferProps<CONF> ? InferProps<CONF>[KEY] : never;



export abstract class BaseOps<
	T extends object,
	CONF
> {
	public abstract connect(db: Knex): void;

	public abstract forEachPropConfig(cb: (propName: KeyOf<T>, config: InferAllPropsType<CONF>) => any): any;
	public abstract getPropConfig<KEY extends KeyOf<T>>(propName: KEY): SubpropertyMerge<InferPropType<CONF, KEY>>;

	public abstract foreignKey(propName: KeyOf<T>): ForeignKey;

	public abstract _protected: {
		config: CONF;
		propEntries: Array<[KeyOf<T>, InferAllPropsType<CONF>]>;

		demandDb(): Knex;
		// keysFor: {
		// 	create: keyof T[],
		// 	notNull: keyof T[],
		// },
		getPropDbColName(propName: KeyOf<T>): string;

		getPropValueType(propName: KeyOf<T>): string | BasicDbValueType | EnumDbValueType;
		getPropValueType(propName: KeyOf<T>, closestBasic?: true): BasicDbValueType | EnumDbValueType;
		getPropValueType(propName: KeyOf<T>, closestBasic?: boolean): string | BasicDbValueType | EnumDbValueType;
	}
}
