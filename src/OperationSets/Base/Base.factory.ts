import { Knex } from "knex";
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { SchemaOpsFactory } from "../KissyFace";

import { BaseConf, BaseOps, BasicDbValueType, InferAllPropsType, PropValueType } from "./Base.io";



export function addBase<
	T extends object,
	ARGS extends Partial<T>,
	CONF
>(): SchemaOpsFactory<BaseConf<T, ARGS>, BaseOps<T, CONF>> {
	return (conf) => {
		/*_protected*/ const propEntries = Object.entries(conf.props) as Array<[KeyOf<T>, InferAllPropsType<CONF>]>;

		if (conf.primaryKey === null && ! conf.multiColumnUniqueIndexes) {
			let warn = [
				`Table "${conf.dbTableName}" has no primary key or multi-column indexes.  This could result in undiscernible duplicate data.`,
				`Suggestion: `,
				`  - add a multiColumnUniqueIndex`
			].join("\n");
			const uniques = propEntries.map(([propName, propConf]) => (propConf as any).unique && propName).filter((it) => !!it);
			if (uniques.length) {
				warn += `\n  - set primaryKey to one of: ${uniques.join()}`
			}
			console.warn(warn);
		}

		/*public*/ function forEachPropConfig(cb: (propName: KeyOf<T>, config: any) => any) {
			propEntries.forEach(([key, config]) => cb(key, config));
		}

		/*public*/ function getPropConfig(propName: KeyOf<T>): any {
			const out = conf.props[propName];
			if (!out) {
				throw new Error(`Prop "${propName}" does not exist on schema "${conf.dbTableName}"`);
			}
			return out;
		}

		/*public*/ function foreignKey(propName: KeyOf<T>) {
			return {
				schema: out,
				propName,
			};
		}

		/*private*/ let _db: Knex;

		/*public*/ function connect(db: Knex, force: boolean = false) {
			if (!!_db && !force) {
				throw new Error(`Database has already been set.  Use connect(db, true) to force new db connection`)
			}
			_db = db;
		}

		/*_protected*/ function demandDb() {
			if (_db === undefined) {
				throw new Error(`Database has not yet been set.  Use connect(db) to set a db connection.`)
			}
			return _db;
		}

		/*protected*/ function getPropDbColName(propName: KeyOf<T>): string {
			return getPropConfig(propName).dbColName || propName;
		}

		/*protected*/ function getPropValueType(propName: KeyOf<T>, closestBasic?: boolean) {
			let valueType: PropValueType = getPropConfig(propName).valueType;
			if (typeof valueType === "object" && "foreignKey" in valueType) {
				let foreignKey = valueType.foreignKey();
				const { schema, propName } = Array.isArray(foreignKey) ? foreignKey[0] : foreignKey;
				valueType = schema.getPropConfig(propName).valueType;
			}

			if (typeof valueType === "object") {
				if ("foreignKey" in valueType) {
					throw new Error(`Foreign key for "${propName}" is also a foreignKey`);

				} else if ("specialType" in valueType) {
					return closestBasic ? valueType.closestBasic : valueType.specialType as BasicDbValueType;

				} else if ("enum" in valueType) {
					return valueType;
				}
			}
			return valueType;
		}

		let out: any;
		return out = {
			connect,
			forEachPropConfig,
			getPropConfig,
			foreignKey,
			_protected: {
				config: conf as any,
				propEntries,
				demandDb,
				getPropDbColName,
				getPropValueType,
			}
		};
	};
}
