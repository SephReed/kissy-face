
import { Repo } from "./Repo";
import { PRE_OPS_ARG, SchemaOpsFactory } from "../KissyFace";
import { RepoToolsConf, RepoToolsOps } from "./RepoTools.io";



export function addRepoTools<
	T extends object = { "Generic is missing from addRepoTools<T>()": true },
	ARGS extends Partial<T> = Partial<T>,
	REPO extends Repo<T, ARGS> = Repo<T, ARGS>
>(): SchemaOpsFactory<
	RepoToolsConf<T, ARGS, REPO>,
	RepoToolsOps<T, ARGS, REPO>
> {
	type PRE_CONF = RepoToolsConf<T, ARGS, any>;
	type PRE_OPS = {};

	return (conf, preOps: PRE_OPS_ARG<PRE_OPS, PRE_CONF>) => {
		const repo: REPO = conf.customRepo ? conf.customRepo(preOps) : new Repo<T, ARGS>(preOps as any) as any;

		// let queryTools: RepoToolsOps<T, ARGS>["queryTools"] = {
		// 	query: repo.query.bind(repo),
		// 	get: repo.get.bind(repo),
		// 	getByFn: repo.getByFn.bind(repo),
		// 	require: repo.require.bind(repo),
		// 	requireByFactory: repo.requireByFactory.bind(repo),
		// 	requireOne: repo.requireOne.bind(repo),
		// 	hydrate: repo.hydrate.bind(repo),
		// 	updateWithObject: repo.updateWithObject.bind(repo),
		// 	syncObject: repo.syncObject.bind(repo),
		// 	update: repo.update.bind(repo),
		// 	updateOne: repo.updateOne.bind(repo),
		// 	updateById: repo.updateById.bind(repo),
		// 	updateByFactory: repo.updateByFactory.bind(repo),
		// 	findOne: repo.findOne.bind(repo),
		// 	find: repo.find.bind(repo),
		// 	deleteById: repo.deleteById.bind(repo),
		// 	deleteManyCautiously: repo.deleteManyCautiously.bind(repo),
		// 	create: repo.create.bind(repo)
		// };
		return {
			// queryTools,
			repo,
			// _protected: {
			// 	convertPropToDbValue: repo.convertPropToDbValue.bind(repo),
			// 	convertPropFromDbValue: repo.convertPropFromDbValue.bind(repo),
			// 	convertFromDbRetrieval: repo.convertFromDbRetrieval.bind(repo),
			// 	convertForDbInsert: repo.convertForDbInsert.bind(repo),
			// 	// repo
			// }
		};
	};
}
