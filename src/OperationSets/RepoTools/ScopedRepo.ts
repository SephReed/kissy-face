import { BaseConf, BaseOps } from "../Base";
import { RepoToolsConf, RepoToolsOps } from ".";
import { PRE_OPS_ARG } from "../KissyFace"
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { Knex } from "knex";
import { OptionalKeys, RequiredKeys } from "../../UtilityTypes/RequiredKeys";


export type RepoSchemaArg<
	T extends object,
	ARGS extends Partial<object>
> = BaseOps<T, BaseConf<T, ARGS> & RepoToolsConf<T, ARGS>> & RepoToolsOps<T, ARGS>


export type QueryResponse<
	T extends object,
	SEL extends Array<KeyOf<T>>
> =
	Omit<Partial<T>, SEL[number]>
	& { [key in Extract<SEL[number], OptionalKeys<T>>]: null | Required<T>[key] }
	& Pick<T, Extract<SEL[number], RequiredKeys<T>>>;




export class ScopedRepo<
	T extends object,
	ARGS extends Partial<object>
> {

	constructor (
		public readonly schema: RepoSchemaArg<T, ARGS>,
		protected _query: () => Knex.QueryBuilder<T>,
	) {
		// just store args
	}

	protected get baseOps(): PRE_OPS_ARG<{}, RepoToolsConf<T, ARGS, any>> {
		return this.schema as any;
	}

	public getPropConfig<KEY extends KeyOf<T>>(key: KEY) {
		return this.baseOps.getPropConfig(key);
	}

	public get config() {  // : CONF_ARG<QueryToolsConf<T, ARGS, any>>
		return this.baseOps._protected.config;
	}

	public convertPropToDbValue<KEY extends KeyOf<T>>(
		propName: KEY,
		value: T[KEY],
		checkForIssues = true,
	) {
		const propConfig = this.getPropConfig(propName);

		if (propConfig.nullable && value === null) {
			return value;
		};

		// pre-conversion
		const valueType = this.baseOps._protected.getPropValueType(propName);
		if (valueType === "date") {
			if (value instanceof Date !== true) {
				const ms = typeof value === "number" ? value : Date.parse(value as any);
				!isNaN(ms) && (value = new Date(ms) as any);
			}
		}

		if (checkForIssues) {
			const errors = [];
			!propConfig.nullable && value === null && errors.push(`property "${propName}" can not be null`);

			if (typeof valueType !== "object") {
				switch (valueType) {
					case "text": typeof value !== "string" && errors.push(`property "${propName}" must be of type "string"`); break;
					case "big_float":
					case "float":
					case "int":
					case "big_int": typeof value !== "number" && errors.push(`property "${propName}" must be of type "number"`); break;
					case "boolean": typeof value !== "boolean" && errors.push(`property "${propName}" must be of type "boolean"`); break;
					case "json": typeof value !== "object" && errors.push(`property "${propName}" must be of type "object"`); break;
					case "date": value instanceof Date || errors.push(`property "${propName}" must be instance of Date`); break;
				}
			} else if(valueType.enum.includes(value as any) === false) {
				errors.push(`property "${propName}" must be of enum type: ${valueType.enum.join()}`)
			}

			if (errors.length) {
				throw `Errors for value "${value}:" ` + errors.join("\n");
			}
		}

		const dbType = this.baseOps._protected.demandDb().client.config.client;
		if (dbType === "sqlite3") {
			if (valueType === "json") {
				return JSON.stringify(value);
			} else if (valueType === "date") {
				return (value as unknown as Date).toISOString();
			}
		}
		return value;
	}

	protected NOT_IN_SCHEMA = Symbol("Prop not in Schema")
	public convertPropFromDbValue<KEY extends KeyOf<T>>(propName: KEY, value: any) {
		let propConfig;
		try {
			propConfig = this.getPropConfig(propName);
		} catch {
			return this.NOT_IN_SCHEMA;
		}
		if (propConfig.nullable && value === null) {
			return value;
		}
		// const dbType = demandDb().client.config.client;
		// if (dbType === "sqlite3") {
		if (propConfig.valueType === "json") {
			try {
				return JSON.parse(value);
			} catch(err) {
				console.warn(`Unable to JSON.parse value "${value}".  Returning null.`);
				return null;
			}
		} else if (propConfig.valueType === "date") {
			try { return new Date(value); }
			catch (err) {
				console.warn(`Unable to create Date from value "${value}".  Returning null.`);
				return null;
			}
		} else if (propConfig.valueType === "boolean") {
			value = !!value;
		}
		// }
		return value;
	}

	public convertForDbInsert(convertMe: Partial<T>, checkForIssues = true): any {
		const out = {} as any;
		const errors: any[] = [];
		Object.entries(convertMe).forEach(([propName, value]) => {
			try {
				out[propName] = this.convertPropToDbValue(propName as any, value, checkForIssues);
			} catch (errs) {
				errors.push(errs);
			}
		});
		if (errors.length) {
			throw `Unable to insert/query ${JSON.stringify(convertMe)} on table "${this.config.dbTableName}:"\n` + errors.join("\n");
		}
		return out;
	}

	protected notInSchemaWarnings = new Map<string, void>()
	public convertFromDbRetrieval(convertMe: any): Partial<T> {
		const out = {} as any;
		Object.entries(convertMe).forEach(([key, value]) => {
			const convertedValue = this.convertPropFromDbValue(key as any, value);
			if (convertedValue === this.NOT_IN_SCHEMA) {
				if (this.notInSchemaWarnings.has(key) === false) {
					this.notInSchemaWarnings.set(key);
					console.warn(`Warning: because the column "${key}" does not exist in the schema, it is being expunged from return.`)
				}
			} else {
				out[key] = convertedValue;
			}
		});
		return out;
	}

	public query(): Knex.QueryBuilder<T> {
		return this._query();
		// return this.baseOps._protected.demandDb()<T>(this.config.dbTableName);
	}

	protected whereId(id: string) {
		const conf = this.config;
		if (conf.primaryKey === null) {
			throw new Error(`Can not select by id on "${conf.dbTableName}" because it has no primaryKey`)
		}
		return { [conf.primaryKey]: id } as unknown as { [key in keyof T]: T[key] };
	}

	public get<SEL extends Array<KeyOf<T>>>(
		id: string,
		select?: SEL
	) {
		return this.findOne(this.whereId(id), select);
	}

	public getByFn<KEY extends KeyOf<T>>(key: KEY) {
		if (!this.getPropConfig(key).unique) {
			throw new Error(`Can not create a getByFn for non-unique key "${key}".  Update the schema for this property to have "unique: true", if it is unique`)
		}
		return async <SEL extends Array<KeyOf<T>>>(
			value: T[KEY],
			select?: SEL
		) => {
			return this.findOne({ [key]: value } as any, select);
		}
	}

	public require<SEL extends Array<KeyOf<T>>>(
		id: string,
		select?: SEL
	) {
		return this.requireOne(this.whereId(id), select);
	}

	public requireByFactory<KEY extends KeyOf<T>>(key: KEY) {
		if (!this.getPropConfig(key).unique) {
			throw new Error(`Can not create a requireByFn for non-unique key "${key}".  Update the schema for this property to have "unique: true", if it is unique`)
		}
		return async <SEL extends Array<KeyOf<T>>>(
			value: T[KEY],
			select?: SEL
		) => {
			return this.requireOne({ [key]: value } as any, select);
		}
	}

	public async requireOne<SEL extends Array<KeyOf<T>>>(
		where: Partial<T>,
		select?: SEL
	) {
		const out = await this.findOne(where, select);
		if (!out) {
			throw new Error(`Could not find a single row meeting the criteria ${JSON.stringify(where)} in table "${this.config.dbTableName}"`);
		}
		return out;
	}

	public async hydrate<
		SEL extends Array<KeyOf<T>>,
		OBJ extends Partial<T>,
	>(
		obj: OBJ,
		hydrations: SEL,
		args?: {
			refresh?: boolean;
		}
	): Promise<OBJ & QueryResponse<T, SEL>> {
		const where = { ...obj };
		Object.keys(where).forEach((key: any) =>
			this.getPropConfig(key).unique || (delete (where as any)[key])
		);
		if (Object.keys(where).length === 0) {
			throw new Error(`Can not hydrate an object which has no unique properties: ${JSON.stringify(obj)}`);
		}
		const fields = (args && args.refresh) ? hydrations : hydrations.filter((propName) => (propName in obj) === false);
		if (fields.length) {
			const updates = await this.requireOne(where, fields);
			for (const [prop, value] of Object.entries(updates)) {
				obj[prop as KeyOf<T>] = value as any;
			}
		}
		return obj as any;
	}

	public async updateWithObject(
		obj: Partial<T>,
		updates: Partial<T>
	) {
		const where = { ...obj };
		Object.keys(where).forEach((key: any) =>
			this.getPropConfig(key).unique || (delete (where as any)[key])
		);
		if (Object.keys(where).length === 0) {
			throw new Error(`Can not update with an object that has no unique properties: ${JSON.stringify(obj)}`);
		}
		// await this.query().where(where).limit(1).update(this.convertForDbInsert(updates));
		await this.update(where, updates, {limit: 1});
		if (obj !== updates) {
			for (const [prop, value] of Object.entries(updates)) {
				obj[prop as KeyOf<T>] = value as T[KeyOf<T>];
			}
		}
		return obj;
	}


	public async syncObject(obj: Partial<T>) {
		return this.updateWithObject(obj, obj);
	}

	public async update(
		where: Partial<T>,
		updates: Partial<T>,
		args?: { limit: number; }
	): Promise<number> {
		if (!updates) { return 0; }  // return {id};
		Object.entries(updates).forEach(([key, value]) => {
			value === undefined && (delete updates[key as KeyOf<T>]);
		});
		if (Object.keys(updates).length === 0) { return 0; }
		const qry = this.query().where(this.convertForDbInsert(where));
		(args && args.limit) && qry.limit(args.limit);
		return qry.update(this.convertForDbInsert(updates));
	}

	public async updateOne(
		where: Partial<T>,
		updates: Partial<T>,
	) {
		return this.update(where, updates, { limit: 1})
	}

	public async updateById(
		id: string,
		values: Partial<T>
	) {
		return this.updateOne(this.whereId(id), values);
	}

	public updateByFactory<KEY extends KeyOf<T>>(key: KEY) {
		if (!this.getPropConfig(key).unique) {
			throw new Error(`Can not create a updateByFn for non-unique key "${key}".  Update the schema for this property to have "unique: true", if it is unique`)
		}
		return async (keyValue: T[KEY], updates: Partial<T>) => {
			return this.updateOne({ [key]: keyValue } as any, updates);
		}
	}


	public async findOne<SEL extends Array<KeyOf<T>>>(
		where: Partial<T>,
		select?: SEL
	) {
		return (await this.find(where, {limit: 1, select}))[0];
	}


	public async find<SEL extends Array<KeyOf<T>>>(
		where: Partial<T>,
		args?: {
			limit?: number,
			select?: SEL,
			// sort?: {[key in KeyOf<T>]?: number}
		}
	): Promise<Array<QueryResponse<T, SEL>>> {
		const qry = this.query();
		if (args) {
			if (args.select) { qry.select(args.select); }
			if (args.limit) { qry.limit(args.limit); }
		}
		const items: Array<QueryResponse<T, SEL>> = await qry.where(this.convertForDbInsert(where));
		return items.map(this.convertFromDbRetrieval.bind(this)) as any;
	}

	public deleteById(id: string, confirm: "BLAME") {
		if (confirm !== "BLAME") {
			throw new Error(`You must be absolutely sure before deletion`);
		}
		return this.query().where(this.whereId(id)).delete();
	}

	public deleteManyCautiously(where: Partial<T>, args: {
		limit: number | "endless",
		confirm: "BLAME"
	}) {
		if (args.confirm !== "BLAME") {
			throw new Error(`You must be absolutely sure before deletion`);
		}
		const qry = this.query().where(where);
		if (args.limit !== "endless") {
			qry.limit(args.limit as number);
		}
		return qry.delete();
	}

	public async create<BASE extends ARGS>(createMe: BASE, forceValues?: "FORCE_VALUES"): Promise<T & BASE> {
		this.baseOps.forEachPropConfig((_prop, conf) => {
			const prop = _prop as keyof ARGS;
			if ("creationFill" in (conf as any)) {
				const { creationFill } = conf as any;
				if (forceValues !== "FORCE_VALUES" || createMe[prop] === undefined) {
					createMe[prop] = creationFill(createMe[prop]) as any;
				}
			}
		});

		const client = this.query().client.config.client;
		const idName = this.config.primaryKey as keyof ARGS;
		const returning = client === 'pg' || client === 'oracledb' || client === 'mssql' ? [idName] : [];

		return this.query()
			.insert(this.convertForDbInsert(createMe), returning as any)
			.then(async (rows) => {
				if (idName) {
					return this.require(createMe[idName] as any);
				}
				if (client === "sqlite3") {
					const rowNum = rows[0];
					return (await this.query().select().where({ rowid: rowNum } as any))[0];
				}
			}) as any;
	}
}
