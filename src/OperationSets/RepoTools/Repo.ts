import { Knex } from "knex";
import { RepoSchemaArg, ScopedRepo } from "./ScopedRepo";



export class Repo<
	T extends object,
	ARGS extends Partial<object>
> extends ScopedRepo<T, ARGS> {

	constructor (
		public readonly schema: RepoSchemaArg<T, ARGS>
	) {
		super(
			schema,
			() => this.baseOps._protected.demandDb()<T>(this.config.dbTableName),
		);

		const out = schema.repo;
		if (!out) { return; }

		if ((out instanceof this.constructor) === false) {
			throw new Error([
				`The repo(${out.constructor.name}) being used by the schema (${schema._protected.config.dbTableName}) is not an instance of this repo class (${this.constructor.name})`,
				`To fix this, update your config for schema "${schema._protected.config.dbTableName}" to include:`,
				`{ customRepo: (schema) => new ${this.constructor.name}(schema) }`,
				""
			].join("\n"))
		}
		return out;
	}

	public forTransaction(tsx: Knex.Transaction<any, any[]>) {
		return new ScopedRepo(this.schema, () => tsx(this.config.dbTableName));
	}
}
