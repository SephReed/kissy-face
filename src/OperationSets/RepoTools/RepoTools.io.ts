import { Repo } from "./Repo";
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { NonNullableKeys } from "../../UtilityTypes/Nullable";
import { RequiredKeys } from "../../UtilityTypes/RequiredKeys";



export type RepoToolsConf<
	T extends object,
	ARGS extends Partial<T>,
	REPO extends Repo<T, ARGS> = Repo<T, ARGS>,
	// KEYS extends KeyOf<T> = KeyOf<T>
	KEYS extends KeyOf<T> = Exclude<NonNullableKeys<T>, RequiredKeys<ARGS>>,  // all non-null fields that aren't required arguments
> = {
	hooks?: any;
	customRepo?: (preOps: any) => REPO,
	props: {
		[key in KEYS]: {
			creationFill(givenValue: any): T[key] | Promise<T[key]>;
		}
	}
}



// ----------------------------------------







// export abstract class QueryTools<
// 	T extends object,
// 	ARGS extends Partial<T>
// > {
// 	public abstract query(): Knex.QueryBuilder<T>;

// 	public abstract get<SEL extends Array<KeyOf<T>>>(
// 		id: string,
// 		select?: SEL
// 	): Promise<null | QueryResponse<T, SEL>>

// 	public abstract getByFn<
// 		KEY extends KeyOf<T>
// 	>(key: KEY): <SEL extends Array<KeyOf<T>>>(value: T[KEY], select?: SEL) => Promise<null | QueryResponse<T, SEL>>;

// 	public abstract require<SEL extends Array<KeyOf<T>>>(
// 		id: string,
// 		select?: SEL
// 	): Promise<QueryResponse<T, SEL>>;

// 	public abstract requireByFactory<
// 		KEY extends KeyOf<T>
// 	>(key: KEY): <SEL extends Array<KeyOf<T>>>(value: T[KEY], select?: SEL) => Promise<QueryResponse<T, SEL>>;

// 	public abstract requireOne<SEL extends Array<KeyOf<T>>>(
// 		where: Partial<T>,
// 		select?: SEL
// 	): Promise<QueryResponse<T, SEL>>;

// 	public abstract hydrate<
// 		SEL extends Array<KeyOf<T>>,
// 		OBJ extends Partial<T>
// 	>(
// 		obj: OBJ,
// 		hydrations: SEL,
// 		args?: {
// 			refresh?: boolean;
// 		}
// 	): Promise<QueryResponse<T, [Extract<RequiredKeys<OBJ>, KeyOf<T>> | SEL[number]]>>;

// 	public abstract updateWithObject(
// 		obj: Partial<T>,
// 		updates: Partial<T>
// 	): Promise<Partial<T>>;

// 	public abstract syncObject(obj: Partial<T>): Promise<Partial<T>>;

// 	public abstract update(
// 		where: Partial<T>,
// 		updates: Partial<T>,
// 		args?: { limit: number; }
// 	): Promise<number>;

// 	public abstract updateOne(
// 		where: Partial<T>,
// 		updates: Partial<T>
// 	): Promise<number>;

// 	public abstract updateById(
// 		id: string,
// 		updates: Partial<T>
// 	): Promise<number>;

// 	public abstract updateByFactory<
// 		KEY extends KeyOf<T>
// 	>(key: KEY): (keyValue: T[KEY], updates: Partial<T>) => Promise<number>;

// 	public abstract findOne<SEL extends Array<KeyOf<T>>>(
// 		where: Partial<T>,
// 		select?: SEL
// 	): Promise<null | (QueryResponse<T, SEL>)>

// 	public abstract find<SEL extends Array<KeyOf<T>>>(
// 		where: Partial<T>,
// 		args?: {
// 			limit?: number,
// 			select?: SEL,
// 			// sort?: {[key in KeyOf<T>]?: number}
// 		}
// 	): Promise<Array<QueryResponse<T, SEL>>>;

// 	public abstract deleteById(id: string, confirm: "BLAME"): Promise<number>;

// 	public abstract deleteManyCautiously(where: Partial<T>, args: {
// 		limit: number | "endless",
// 		confirm: "BLAME"
// 	}): Promise<number>

// 	public abstract create<BASE extends ARGS>(createMe: BASE, forceValues?: "FORCE_VALUES"): Promise<T & BASE>;
// }



export interface RepoToolsOps<
	T extends object,
	ARGS extends Partial<T>,
	REPO extends Repo<T, ARGS> = Repo<T, ARGS>
> {
	// queryTools: QueryTools<T, ARGS>;
	repo: REPO;
	// _protected: {
	// 	convertPropToDbValue<KEY extends KeyOf<T>>(propName: KEY, value: T[KEY]): any;
	// 	convertPropFromDbValue<KEY extends KeyOf<T>>(propName: KEY, value: any): T[KEY];
	// 	convertForDbInsert(convertMe: Partial<T>): any;
	// 	convertFromDbRetrieval(convertMe: any): Partial<T>;
	// 	// repo: Repo<T, ARGS>
	// }
}
