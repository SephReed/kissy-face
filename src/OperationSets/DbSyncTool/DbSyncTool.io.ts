import { DbSyncPermit, DbSyncTool } from "../../DbSyncTool/DbSyncTool";
import { KeyOf } from "../../UtilityTypes/KeyOf";
import { PropValueType } from "../Base/Base.io";



export type DbSyncToolConf<
	T extends object,
	KEY extends string = KeyOf<T>
> = {
	props: {
		[key in KEY]: {
			valueType: PropValueType;
		}
	},
	syncPermits: Array<DbSyncPermit>
}




// ------------------------------------


export abstract class DbSyncToolOps {
	public abstract dbSyncTool(): DbSyncTool;
}
