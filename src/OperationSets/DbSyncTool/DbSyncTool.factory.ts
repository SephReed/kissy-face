
import { CONF_ARG, PRE_OPS_ARG, SchemaOpsFactory } from "../KissyFace";
import { DbSyncToolConf, DbSyncToolOps } from "./DbSyncTool.io";
import { RepoToolsOps } from "../RepoTools/RepoTools.io";
import { DbSyncTool } from "../../DbSyncTool/DbSyncTool";
import { IColumn } from "../../DbSyncTool/Schema";



export function addDbSyncTool<
	T extends object = { "Generic is missing from addDbSchemaTools<T>()": true },
>(): SchemaOpsFactory<
	DbSyncToolConf<T>,
	DbSyncToolOps
> {
	type PRE_CONF = DbSyncToolConf<T, string>;
	type PRE_OPS = RepoToolsOps<T, any>;

	return (_conf, _preOps: PRE_OPS_ARG<PRE_OPS, PRE_CONF>) => {
		const conf = _conf as any as CONF_ARG<PRE_CONF>;
		const preOps = _preOps as Exclude<typeof _preOps, undefined>;
		const { demandDb, propEntries } = preOps._protected;


		/*private*/ let _dbSyncTool: DbSyncTool;
		/*public*/ function dbSyncTool() {
			if (_dbSyncTool) { return _dbSyncTool; }
			const { syncPermits } = conf;
			const db = demandDb();

			return _dbSyncTool = new DbSyncTool({
				db,
				tableName: conf.dbTableName,
				permits: syncPermits,
				expectedSchema: {
					tableName: conf.dbTableName,
					multiColumnUniqueIndexes: conf.multiColumnUniqueIndexes?.map((it) => {
						return {
							id: null,
							columns: it
						}
					}) || [],
					columns: propEntries.map(([propName, propConf]) => {
						let foreignKeys: IColumn["foreignKeys"] = [];
						if (typeof propConf.valueType === "object" && "foreignKey" in propConf.valueType) {
							const foreignKey = propConf.valueType.foreignKey();
							(Array.isArray(foreignKey) ? foreignKey : [foreignKey]).forEach((item) => {
								foreignKeys.push({
									table: item.schema._protected.config.dbTableName,
									column: item.schema._protected.getPropDbColName(item.propName),
								})
							})
						}
						return {
							name: propConf.dbColName || propName,
							unique: !!propConf.unique,
							nullable: !!propConf.nullable,
							primaryKey: propName === conf.primaryKey,
							type: preOps._protected.getPropValueType(propName),
							foreignKeys,
							indexes: [],
						}
					})
				}
			});
		}


			// 		const valueType = preOps._protected.getPropValueType(propName);
			// 		if (dbConfig.type !== valueType) {
			// 			const canIgnore = permits.find(({issue}) => issue === "has-datatype-mismatch");
			// 			!canIgnore && errors.push([
			// 				`Column "${dbColName}" is of type ${JSON.stringify(dbConfig.type)} in database, but `
			// 				+ `of type ${JSON.stringify(valueType)} in the schema.`,
			// 				`- To update the database schema, add { modifyColumn: "${dbColName}", mod: "change-type", to: "${valueType}"} to migrationPermits.`
			// 			].join("\n"));
			// 		}
			// 	}
			// });


		return {
			dbSyncTool
		};


		// 				if (dbType() === "sqlite3") {
		// 					throw new Error([
		// 						`Column "${colName}" can not have its type changed because sqlite3 does not support it.`,
		// 						`Fortunately, sqlite3 is loosely typed so you can replace your "change-type" permit with { ignoreColumn: "${colName}", issue: "has-datatype-mismatch" } and things will work just fine.`
		// 					].join("\n"));
		// 				}
		// 				throw new Error("Not yet implemented change-type");

	};
}
