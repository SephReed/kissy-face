import { SubpropertyMerge } from "../UtilityTypes/SubpropertyMerge";
import { DeepIntersectUnion, UnionToIntersection } from "../UtilityTypes/UnionToIntersection";
import { addBase } from "./Base/Base.factory";
import { BaseConf, BaseOps } from "./Base/Base.io";


export type CONF_ARG<PRE_CONF> = SubpropertyMerge<BaseConf<any, any> & PRE_CONF>;
export type PRE_OPS_ARG<PRE_OPS, PRE_CONF> = SubpropertyMerge<
	BaseOps<any, SubpropertyMerge<BaseConf<any, any> & PRE_CONF>>
	& PRE_OPS
>

// export type KissyFaceConf<
// 	T extends object,
// 	ARGS extends Partial<T>,
// 	SUB_CONFS extends object
// > = BaseConf<T, ARGS> & SUB_CONFS;


export type SchemaOpsFactory<
	CONF,
	OPS_TO_ADD
> = (
	conf: CONF,
	preOps?: any
) => OPS_TO_ADD;


export function KissyFace<
	T extends object = { "The first generic is required on KissyFace<T>()": true },
	ARGS extends Partial<T> = Partial<T>,
>() {
	return <RES extends SchemaOpsFactory<any, any>>(chain: Array<RES>) => {

		type InferConf<RES> = RES extends SchemaOpsFactory<infer OUT, any> ? OUT : never;
		type InferOps<RES> = RES extends SchemaOpsFactory<any, infer OUT> ? OUT : never;

		type CONF = BaseConf<T, ARGS> & UnionToIntersection<InferConf<RES>>

		type OPS = BaseOps<T, CONF> & UnionToIntersection<InferOps<RES>>

		return (conf: CONF) => {
			const ops = addBase()(conf as any, {} as any);

			const deepMerge = (source: object, target: object) => {
				Object.entries(source).forEach(([_key, value]) => {
					const key = _key as keyof typeof target;
					if (key in target && typeof value === "object") {
						deepMerge(value, target[key])
					} else {
						target[key] = source[key]
					}
				})
			}
			chain.forEach((factory: RES) => {
				deepMerge(factory(conf, ops), ops);
			});
			return ops as unknown as OPS;
		}
	}
}
